<style>
    .centre-language{
        text-transform: capitalize;
    }
    .selectize-control.single .selectize-input::after, .selectize-control.multi .selectize-input::after,
    .selectize-control.single .selectize-input.input-active::after,
    .selectize-control.multi .selectize-input.input-active::after{
        content: none;
    }
    .gray{
        color: #1f78aa;
    }
    .centre_color{
        color: #aeabae;
    }
    @media (max-width: 580px){
        .centre-language, .centre{
            border-radius: 0;
            background-color: #fff;
        }
    }
    @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
        .sticky-wrapper{
            height: 104px !important;
        }
        .filter{
            position: absolute !important;
            padding: 30px 0 !important;
            margin-top: -60px;
        }
    }
    @media screen and (min-width:0\0) {
        .sticky-wrapper{
            height: 104px !important;
        }
        .filter{
            position: absolute !important;
            padding: 30px 0 !important;
        }
    }
    @media (min-width: 1199px) and (max-width:1365px){
        .carousel .item{
            min-height: 600px;
        }
    }
    @media (min-width: 992px) and (max-width:1199px){
        .carousel .item{
            min-height: 550px;
        }
    }
    @media (min-width: 768px) and (max-width:991px){
        .carousel .item{
            min-height: 450px;
        }
    }
    @media (max-width: 767px){
        .carousel .item{
            min-height: 350px;
        }
    }
    @media (max-width:580px){
        .carousel .item{
            min-height: 200px;
        }
        #carousel-example-generic{
            margin-top:290px;
        }
    }
/*@media (min-width: 900px) and (max-width: 1366px) {
    .carousel .item{
        min-height: 600px;
    }
}*/
</style>
<!--[if gte IE 10]>
<style type="text/css">
    .filter{
        position: absolute !important;
    }
</style>
<![endif]-->
<!--Slider Begin-->
<section class="go-slider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            for ($i=0; $i <$count ; $i++) { 
                ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class="<?=$i==0?'active':''?>"></li>
                <?php
            }
            ?>   
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $i=0;
            foreach ($images as $image) {
                $i++;
                ?>
                <!-- First slide -->
                <div class="item <?=$i==1?'active':''?>">
                    <img src="<?= base_url($image->avatar) ?>" alt="" class="img-responsive" />
                    <div class="carousel-caption">
                    <!--<div class="icon-container" data-animation="animated fadeInDownBig">
                    <i class="fa fa-building-o"></i>
                </div>-->
                <h1 data-animation="animated bounceInRight">
                    <?=$image->title?>
                </h1>
                <h4 data-animation="animated bounceInLeft">
                    <?=$image->description?>
                </h4>
            </div>
        </div>
        <!-- /.item -->
        <?php
    }
    ?>

</div>
<!-- /.carousel-inner -->
</div>
<!-- /.carousel -->
<section class="filter">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <form action="<?= site_url('search') ?>" class="form-inline filter-form" method="POST">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3">
                            <input type="text" name="query" placeholder="Enter event name, centre name, preacher or place">
                        </div>
                        <div class="form-group col-xs-3 col-sm-3">
                            <select name="centre" class="centre gray" onChange="this.style.color='#000';">
                                <option value="" class="gray" hidden="">Type of retreat</option>
                                <option value="0" class="centre_color">All</option>
                                <?php
                                foreach ($searchtype as $val) {
                                    ?>

                                    <option value="<?= $val->id ?>" class="centre_color"><?= $val->name ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-xs-3 col-sm-2">
                            <select name="centre_lang" class="centre-language gray" onChange="this.style.color='#000';">
                                <option value="" class="gray" hidden="">Language</option>
                                <option value="0" class="centre_color">Any</option>
                                <?php
                                foreach ($searchlanguage as $val) {
                                    ?>

                                    <option value="<?= $val->id ?>" class="centre_color"><?= $val->language ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-xs-3 col-sm-2">
                            <input type="text" name="startdate" class="start-date" placeholder="Start Date" />
                        </div>
                        <div class="form-group col-xs-3 col-sm-2">
                            <input type="text" name="enddate" class="end-date" placeholder="End Date" />
                        </div>
                        <div class="form-group col-xs-12 col-sm-1 search-button">
                            <button type="submit" class="btn btn-primary btn-search submit-name"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</section>
<!--Slider End-->
<!-- <script type="text/javascript">
    $(document).ready(function() { 
        $('button[type="submit"]').prop('disabled', true);
        $('input[type="text"]').keyup(function() {
          if($(this).val() != '') {
             $('button[type="submit"]').prop('disabled', false); 
         } 
     }); 
        $('select').change(function() {
          if($(this).val() != '') {
             $('button[type="submit"]').prop('disabled', false); 
         } 
     });
 });
</script> -->
