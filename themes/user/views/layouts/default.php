<!DOCTYPE html>
<html  itemscope itemtype="http://schema.org/WebSite" lang="en">

<?php echo $template['partials']['head']; ?>

<body class="<?=(isset($body_class) && $body_class)?$body_class:''?>" data-spy="scroll" data-target="#myScrollspy">

	<?php echo $template['partials']['header']; ?>
	<?php echo $template['body']; ?>
	<?php echo $template['partials']['footerbanner']; ?>
	<?php echo $template['partials']['footer']; ?>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-96103406-1', 'auto');
		ga('send', 'pageview');

	</script>

</body>

</html>
