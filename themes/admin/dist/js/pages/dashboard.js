/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/
 "use strict";

 var RCdashBoardinit = function () {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


    $('.daterange').daterangepicker(
    {
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
            'Last 7 Days': [moment().subtract('days', 6), moment()],
            'Last 30 Days': [moment().subtract('days', 29), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        },
        startDate: moment().subtract('days', 29),
        endDate: moment()
    },
    function (start, end) {
        alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });

    /* jQueryKnob */
    $(".knob").knob();

    function drawMap(data){

        var data = JSON.parse(data);


        /* Morris.js Charts */
        // Sales chart
        var area = new Morris.Line({
            element: 'revenue-chart',
            resize: true,
            data: data.area,
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Visitors'],
            dateFormat:function (x) { var date=new Date(x); return months[date.getMonth()]+' '+ date.getFullYear();},
            xLabelFormat: function (x) { return months[x.getMonth()]+' '+ x.getFullYear(); },
            lineColors: ['#efefef'],
            xLabels:'month',
            lineWidth: 2,
            hideHover: 'auto',
            gridTextColor: "#fff",
            gridStrokeWidth: 0.4,
            pointSize: 4,
            pointStrokeColors: ["#efefef"],
            gridLineColor: "#efefef",
            gridTextFamily: "Open Sans",
            gridTextSize: 10
        });
        
        
        /* Morris.js Charts */
        // Booking chart
        var Booking1 = new Morris.Line({
            element: 'booking-chart',
            data: data.bookingData,
            xkey: 'y',
            ykeys: ['INR','USD'],
            labels: ['Rs.','US $'],
            dateFormat:function (x) { var date=new Date(x); return months[date.getMonth()]+' '+ date.getFullYear();},
            xLabelFormat: function (x) { return months[x.getMonth()]+' '+ x.getFullYear(); },
            lineColors: ['#efefef'],
            xLabels:'month',
            lineWidth: 2,
            hideHover: 'auto',
            gridTextColor: "#fff",
            gridStrokeWidth: 0.2,
            pointSize: 4,
            pointStrokeColors: ["#efefef"],
            gridLineColor: "#efefef",
            gridTextFamily: "Open Sans",
            gridTextSize: 10
        });

        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: data.line,
            xkey: 'y',
            ykeys: ['count'],
            labels: ['Bookings'],
            dateFormat:function (x) { var date=new Date(x); return months[date.getMonth()]+' '+ date.getFullYear();},
            xLabelFormat: function (x) { return months[x.getMonth()]+' '+ x.getFullYear(); },
            lineColors: ['#efefef'],
            xLabels:'month',
            lineWidth: 2,
            hideHover: 'auto',
            gridTextColor: "#fff",
            gridStrokeWidth: 0.4,
            pointSize: 4,
            pointStrokeColors: ["#efefef"],
            gridLineColor: "#efefef",
            gridTextFamily: "Open Sans",
            gridTextSize: 10
        });

//World map by jvectormap
$('#world-map').vectorMap({
    map: 'world_mill_en',
    backgroundColor: "transparent",
    regionStyle: {
      initial: {
        fill: '#e4e4e4',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
    }
},
series: {
  regions: [{
    values: data.visitorsData,
    labels: ['Bookings'],
    scale: ["#92c1dc", "#aaffff"],
    normalizeFunction: 'polynomial'
}]
},
onRegionLabelShow: function (e, el, code) {
    if (typeof data.visitorsData[code] != "undefined")
        el.html(el.html() + ': ' + data.visitorsData[code] + ' Bookings');
}
});
}

function worldMapHits(mapEl,data){
    var data = JSON.parse(data);
    mapEl.vectorMap({
        map: 'world_mill_en',
        backgroundColor: "transparent",
        regionStyle: {
          initial: {
            fill: '#e4e4e4',
            "fill-opacity": 1,
            stroke: 'none',
            "stroke-width": 0,
            "stroke-opacity": 1
        }
    },
    series: {
      regions: [{
        values: data.siteHits,
        labels: ['Site Views'],
        scale: ["#92c1dc", "#aaffff"],
        normalizeFunction: 'polynomial'
    }]
},
onRegionLabelShow: function (e, el, code) {
  if (typeof data.siteHits[code] != "undefined")
    el.html(el.html() + ': ' + data.siteHits[code] + ' Views');
}
});
}

function ajaxRequests(url){
    $.post(url, function (data) {
        drawMap(data);
        // default per-day hits
        $("#day").trigger('click');
    });
}

ajaxRequests(SITE_URL + "/dashboard/get_chart_info");

$('#day').on('click', function(){
    var that = $("#day-view-map");
    //clicked tabs active
    $(this).closest('div').find('.active').removeClass('active');
    $(this).addClass('active');
    //fetch hits details per day
    $.post(SITE_URL + "/dashboard/hits_per_day", function(data){
        //keep other maps hidden
            $("#week-view-map,#month-view-map").css({height: 0}).hide();
        if(that.hasClass('rendered')){

            that.css({height: '400px'}).show();

        }else{
          $("#day-view-map").css({height: '400px'}).addClass('rendered').show();
          //draw map
          worldMapHits(that, data);  
      }
  });
});

$('#week').on('click', function(){
    var that = $("#week-view-map");
    $(this).closest('div').find('.active').removeClass('active');
    $(this).addClass('active');
    $.post(SITE_URL + "/dashboard/hits_per_week", function(data){
            $("#day-view-map,#month-view-map").css({height: 0}).hide();
        if(that.hasClass('rendered')){
            that.css({height: '400px'}).show();
        }else{
          $("#week-view-map").css({height: '400px'}).addClass('rendered').show();
          worldMapHits(that, data);
      }
      
  });
});

$('#month').on('click', function(){
    var that = $("#month-view-map");
    $(this).closest('div').find('.active').removeClass('active');
    $(this).addClass('active');
    $.post(SITE_URL + "/dashboard/hits_per_month", function(data){
            $("#week-view-map,#day-view-map").css({height: 0}).hide();
        if(that.hasClass('rendered')){
            that.css({height: '400px'}).show();
        }else{
           $("#month-view-map").css({height: '400px'}).addClass('rendered').show();
           worldMapHits(that,data);
       }
       
   });
});


}