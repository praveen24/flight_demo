<!DOCTYPE html>
<html>

<?php echo $template['partials']['head']; ?>
<?php // echo $template['partials']['headerscript']; ?>

<body  class="skin-blue sidebar-mini wysihtml5-supported">
    <div id="wrapper" class="fluid" style="background-color: #1C5B80">

        <?php echo $template['partials']['header']; ?>

        <aside class="main-sidebar">
            <?php echo $template['partials']['sidebar']; ?>
        </aside>
        
        <div class="content-wrapper">
            <?php echo $template['body']; ?>
        </div>

        <?php echo $template['partials']['footer']; ?>
        
        
    </div>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-96103406-1', 'auto');
      ga('send', 'pageview');

  </script>


</body>

</html>
