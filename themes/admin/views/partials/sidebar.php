
<!-- sidebar -->
<section class="sidebar">

    <ul class="sidebar-menu">


        <li <?php echo ( in_array($current_controller, array('user'))) ? 'class="active treeview"' : 'treeview'; ?>>
            <a href="<?= site_url('user') ?>">
                <i class="fa fa-list"></i>
                <span>MPL</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li <?php echo ( in_array($current_controller_method, array('service'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('mpl/service') ?>"><i class="fa fa-line-chart"></i> Service</a></li>
                <li <?php echo ( in_array($current_controller_method, array('station'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('mpl/station') ?>"><i class="fa fa-bar-chart"></i> Station</a></li>
                <li <?php echo ( in_array($current_controller_method, array('class'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('mpl/class') ?>"><i class="fa fa-user-plus"></i> Class</a></li>
                <li <?php echo ( in_array($current_controller_method, array('packet'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('mpl/packet') ?>"><i class="fa fa-pie-chart"></i> Packet</a></li>
                <li <?php echo ( in_array($current_controller_method, array('spec_table'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('mpl/spec_table') ?>"><i class="fa fa-pie-chart"></i> Spec Table</a></li>

            </ul>
        </li>
        <li <?php echo ( in_array($current_controller, array('user'))) ? 'class="active treeview"' : 'treeview'; ?>>
            <a href="<?= site_url('user') ?>">
                <i class="fa fa-group"></i>
                <span>Reports</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li <?php echo ( in_array($current_controller_method, array('disbursement_summary'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/disbursement_summary') ?>"><i class="fa fa-line-chart"></i> Disbursement Summary Report</a></li>
                <li <?php echo ( in_array($current_controller_method, array('budget_status'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/budget_status') ?>"><i class="fa fa-bar-chart"></i> Budget Status</a></li>
                <li <?php echo ( in_array($current_controller_method, array('cpp'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/cpp') ?>"><i class="fa fa-user-plus"></i> CPP</a></li>
                <li <?php echo ( in_array($current_controller_method, array('payment'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/payment') ?>"><i class="fa fa-pie-chart"></i> Payment</a></li>
                <li <?php echo ( in_array($current_controller_method, array('no_of_pax'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/no_of_pax') ?>"><i class="fa fa-area-chart"></i> No. of Pax</a></li>
                <li <?php echo ( in_array($current_controller_method, array('meal_overage'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/meal_overage') ?>"><i class="fa fa-line-chart"></i> Meal Overage</a></li>
                <li <?php echo ( in_array($current_controller_method, array('no_of_flight'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/no_of_flight') ?>"><i class="fa fa-bar-chart"></i> No. of Flight</a></li>
                <li <?php echo ( in_array($current_controller_method, array('cost_by_item'))) ? 'class="active"' : 'treeview'; ?>><a href="<?= site_url('reports/cost_by_item') ?>"><i class="fa fa-pie-chart"></i> Cost by item/ SVC Level</a></li>

            </ul>
        </li>
        <li <?php echo ( in_array($current_controller, array('documentation'))) ? 'class="active treeview"' : 'treeview'; ?>>
            <a href="<?= site_url('documentation') ?>">
                <i class="fa fa-file-text-o"></i>
                <span>Documentation</span>
            </a>
        </li>
    </ul>
</section>
<!-- /.sidebar -->
