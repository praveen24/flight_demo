#
# TABLE STRUCTURE FOR: accomodation
#

DROP TABLE IF EXISTS accomodation;

CREATE TABLE `accomodation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accomodation_type` varchar(80) DEFAULT NULL,
  `currency` int(11) DEFAULT '98',
  `is_global` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (1, 'A/C Room', 98, 1, '2016-01-28 10:13:47');
INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (2, 'Double Room', 98, 1, '2016-01-28 10:14:01');
INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (3, 'Single Room', 98, 1, '2016-01-28 10:14:18');
INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (4, 'Dormitory', 98, 1, '2016-04-04 12:08:00');


#
# TABLE STRUCTURE FOR: center
#

DROP TABLE IF EXISTS center;

CREATE TABLE `center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street_address1` varchar(100) NOT NULL,
  `street_address2` varchar(100) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `key_person` varchar(160) DEFAULT NULL,
  `established` varchar(160) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `key_preacher` varchar(80) DEFAULT NULL,
  `facility` text,
  `contact` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `logo` varchar(250) NOT NULL,
  `reg_num` varchar(200) NOT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL,
  `verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `popularity` int(11) NOT NULL DEFAULT '0' COMMENT '1-popular, 0-nonpopular',
  `upgraded_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logitude` varchar(30) NOT NULL,
  `lattitude` varchar(30) NOT NULL,
  `rc_type_id` int(11) NOT NULL,
  `rc_category_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL COMMENT '1=yes 0=no',
  `deleted_on` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rc_type_id` (`rc_type_id`),
  KEY `rc_category_id` (`rc_category_id`),
  FULLTEXT KEY `name` (`name`,`country`,`state`,`city`,`description`,`street_address1`,`street_address2`,`zipcode`),
  FULLTEXT KEY `name_2` (`name`,`country`,`state`,`city`,`street_address1`,`street_address2`,`zipcode`,`description`),
  CONSTRAINT `center_ibfk_1` FOREIGN KEY (`rc_type_id`) REFERENCES `rc_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

INSERT INTO center (`id`, `name`, `country`, `state`, `city`, `street_address1`, `street_address2`, `zipcode`, `key_person`, `established`, `website`, `key_preacher`, `facility`, `contact`, `email`, `logo`, `reg_num`, `description`, `status`, `rank`, `verified`, `verified_at`, `popularity`, `upgraded_at`, `logitude`, `lattitude`, `rc_type_id`, `rc_category_id`, `is_deleted`, `deleted_on`, `timestamp`) VALUES (54, 'Kallar Retreat Center', 'India', 'Kerala', 'Thiruvananthapuram', 'Beach Road1', 'Kallar', '695551', NULL, '1943', 'www.kottayamad.org', 'Bishop Charles Lavigne', 'The Eparchy of Kottayam was erected exclusively for the Southist (Knanaya) Catholics in 1911. The Knanaya Community traces its origin from a group of Jewish-Christian emigrants from Southern Mesopotamia to the South Indian port of Cranganore in AD 345, who formed themselves into an endogamous community. They co-existed peacefully in the Indian nation and fulfilled their missionary purpose of re-invigorating the Church of St Thomas Christians. The original community consisted of about 400 persons belonging to 72 families of seven septs headed by Thomas of Kynai. A bishop by name Uraha Mar Yousef, four priests and several deacons were among them.', '+91 481 2563527', 'kallar@goretreat.in', 'uploads/image/center_logo/ananda10.jpg', '17/2014', 'The Knanaya Community traces its origin back to a Jewish-Christian immigrant community. They migrated from Southern Mesopotamia to the Malabar ( present Kerala ) Coast of Cranganore ( Kodungalloor ) in AD 345 under the leadership of an enterprising merchant Thomas of Cana ( Knai Thomman ). This migration is considered as a turning point in the history of St Thomas Christians of Malabar.  The original community consisted of about 400 persons belonging to 72 families of seven clans. A bishop by name Uraha Mar Yousef, four priests and several deacons were among them. They formed themselves into an endogamous community by keeping their tradition and culture but co-existed peacefully among the St. Thomas Christians in India.These colonists  were welcomed by Cheraman Perumal, then king, and were given permission to settle down in Kodungalloor. Later, Cheraman Perumal bestowed them with 72 royal privileges, and it was recorded on copper plates ( Knai Thomman Cheppedu ). According to the Copper plates, these privileges were given to Thomas and his colleagues and all of his descendents as long as the sun and moon exist. These privileges are very important since all these privileges had influenced the community’s social life as well as the social status in the past years.\r\n\r\nAll knananites were Syrian Christians until the historic ‘Koonan Kurisu Sathyam’ (A pledge by about 25000 Syrian Christians held on to a rope tied to a leaning cross) when some of them accepted Jacobite faith and joined Jacobite/Orthodox Church; others remained with the Roman Catholic faith. However, they continued to keep their culture, tradition and practicing endogamy through the centuries.\r\n\r\nOn August 29, 1911 a new Vicariate Apostolic of Kottayam was erected exclusively for the Knanaya Community by the apostolic letter “In Universi Christiani” of His Holiness Pope St Pius X and Mar Mathew Makil was transferred to Kottayam as Vicar Apostolic. On December 21, 1923 the Vicariate Apostolic of Kottayam was raised to an Eparchy by Pope Pius XI. Pope John Paul II by a sovereign decision on December 23, 2003 reconfirmed its identity and allowed to maintain the status quo of the Archdiocese of Kottayam. On May 9, 2005 the Major Archbishop Mar Varkey Cardinal Vithayathil issued the Decree “The Eparchy of Kottayam,” elevating the Eparchy of Kottayam to the rank of a Metropolitan See, and appointed Mar Kuriakose Kunnacherry as the first Metropolitan of the newly erected Metropolitan See of Kottayam.', 1, 0, 1, '2016-08-22 12:24:26', 1, '0000-00-00 00:00:00', '77.12910119999992', '8.7098382', 19, 0, 0, '0000-00-00 00:00:00', '2016-07-15 12:12:07');
INSERT INTO center (`id`, `name`, `country`, `state`, `city`, `street_address1`, `street_address2`, `zipcode`, `key_person`, `established`, `website`, `key_preacher`, `facility`, `contact`, `email`, `logo`, `reg_num`, `description`, `status`, `rank`, `verified`, `verified_at`, `popularity`, `upgraded_at`, `logitude`, `lattitude`, `rc_type_id`, `rc_category_id`, `is_deleted`, `deleted_on`, `timestamp`) VALUES (55, 'Francis Retreat Center', 'India', 'Kerala', 'Ernakulam', 'Kaloor', 'Ernakulam', '682017', NULL, '1890', 'testweb.tfuj', 'Dr. Jaison', 'test facility', '9567254707', 'mail@mail123q.coo', './uploads/images/centreslider/57be95cf643e2.png', '18/2000', 'test description', 1, 0, 1, '2016-08-25 12:23:16', 1, '0000-00-00 00:00:00', '76.29989679999994', '9.986834199999999', 21, 0, 0, '0000-00-00 00:00:00', '2016-08-25 12:23:03');


