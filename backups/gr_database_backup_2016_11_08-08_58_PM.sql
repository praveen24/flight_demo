#
# TABLE STRUCTURE FOR: accomodation
#

DROP TABLE IF EXISTS accomodation;

CREATE TABLE `accomodation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accomodation_type` varchar(80) DEFAULT NULL,
  `currency` int(11) DEFAULT '98',
  `is_global` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (1, 'A/C Room', 98, 1, '2016-01-28 10:13:47');
INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (2, 'Double Room', 98, 1, '2016-01-28 10:14:01');
INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (3, 'Single Room', 98, 1, '2016-01-28 10:14:18');
INSERT INTO accomodation (`id`, `accomodation_type`, `currency`, `is_global`, `created_at`) VALUES (4, 'Dormitory', 98, 1, '2016-04-04 12:08:00');


