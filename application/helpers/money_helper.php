<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function convert_money($amount, $from, $to) {
//    $content = file_get_contents("http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22" . $from . $to . "%22%29&format=json&env=store://datatables.org/alltableswithkeys");
//    $data1 = json_decode($content);
//    $rateval = $data1->query->results->rate->Rate;
    if ($from == $to) {
        return $amount;
    }
    $content = file_get_contents("http://www.google.com/finance/info?q=CURRENCY%3a" . $from . $to);
    $content = str_replace('//', '', $content);
    $data = json_decode($content);
    $rateval = $data[0]->l;
    $value = floatval($rateval) * floatval($amount);
    return $value;
}

function get_rate_money($from, $to) {
//    $content = file_get_contents("http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22" . $from . $to . "%22%29&format=json&env=store://datatables.org/alltableswithkeys");
//    $data1 = json_decode($content);
//    $rateval = $data1->query->results->rate->Rate;
    if ($from == $to) {
        return 1;
    }
    $content = file_get_contents("http://www.google.com/finance/info?q=CURRENCY%3a" . $from . $to);
    $content = str_replace('//', '', $content);
    $data = json_decode($content);
    $rateval = $data[0]->l;
    return $rateval;
}


  if (!function_exists('format_money')) {

       function format_money($money, $currency = '', $precision = 2)
    {
        setlocale(LC_MONETARY, 'en_US');
        return $currency .' '. number_format(round($money, $precision),2,'.',',') ;
    }

  }
