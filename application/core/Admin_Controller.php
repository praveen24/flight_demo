<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('gr_auth', IMS_DB_PREFIX . 'user');
        $this->gr_auth->set_login_mode('system');
        if (!$this->gr_auth->is_admin_logged()) {
            if ($this->input->is_ajax_request()) {
                echo "<script type='text/javascript'>$(function(){window.location='" . site_url('system') . "';});</script>";
                exit;
            } else {
                redirect(site_url('system'));
            }
        }
        $this->load->library('form_validation');
        $this->load->model(array('admin_common_model','dashboard/dashboard_model'));
        $this->data['key_roles'] = array(1, 4, 5);
        

        $this->gr_template
        ->enable_parser(FALSE)
        ->set_theme('admin')
        ->set_layout('default')
        ->title("Admin's Portal")
        ->set_partial('head')
        ->set_partial('header_scroll')
        ->set_partial('header')
        ->set_partial('sidebar')
        ->set_partial('footer')
        ->set_partial('control_sidebar')
        ->set_partial('headerscript');
        $this->data['theme_path'] = 'themes/admin/';
        $gr_admin = FALSE;
        $roledata=$this->session->userdata('current_centre_role');
        if(isset($roledata->role_id)&&$roledata->role_id == 1){
            $gr_admin = TRUE;
        }
        
        $this->form_validation->set_error_delimiters('<label generated="true" class="error">', '</label>');
        $this->breadcrumb->append_crumb(lang('dashboard'), site_url('dashboard'), false);
        $this->data['current_controller'] = $this->router->fetch_class();
        $this->data['current_controller_method'] = $this->router->fetch_method();
        $this->data['approved_count'] = $this->admin_common_model->approve_preacher_count();
        $this->data['current_user'] = $this->current_user = $this->admin_common_model->getCurrentUser($this->gr_auth->user_id());
        $this->data['mailbox'] = $this->admin_common_model->getUnreadMessages($this->gr_auth->user_id(), $gr_admin);
        $this->data['unreadMail'] = $this->admin_common_model->getUnreadMails($this->gr_auth->user_id(), $gr_admin);
        if (!$this->session->userdata('current_centre_role')) {
            $keyRoles = $this->admin_common_model->getKeyRoles($this->current_user->id);
            $this->session->set_userdata('current_centre_role', $keyRoles[0]);
            if (!empty($keyRoles)):
                else:
                    endif;
            }
            global $_current_centre_role;
            $_current_centre_role = $this->current_centre_role = $this->session->userdata('current_centre_role');

            global $_permissions;
            $_permissions = $this->_permissions = $this->admin_common_model->getUserPermissions($this->current_centre_role->role_id);

            if (!$this->current_centre_role) {
                show_error('No roles defined for this user. Please contact system admin.');
            }

            if (_is("RC Admin") || _is("Event Admin")) {
                $center = $this->session->userdata('current_centre_role')->center_id;
                $this->data['checkVerified'] = $this->dashboard_model->checkVerified($center);
            }

            $roles = $this->admin_common_model->getSingleUserRole($this->gr_auth->user_id());
            $checkRoleIsMaster = $this->admin_common_model->checkRoleIsMaster($roles->role_id);
            if($checkRoleIsMaster->is_master == 1){
                $this->data['isMaster'] = 1;
            }else{
                $this->data['isMaster'] = 0;
            }
        }

    }

