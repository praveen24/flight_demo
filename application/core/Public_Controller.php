<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Public_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('gr_auth', IMS_DB_PREFIX . 'customer');
        $this->load->model(array('admin_common_model'));
        if ($this->uri->segment(2) != 'change_pass_page') {
            $this->data['current_user'] = $this->current_user = $this->admin_common_model->getCurrentCustomer($this->gr_auth->customer_user_id());
        }
        $ip = $this->input->ip_address();
        // $ip = '202.83.47.55';
        $is_visited = $this->admin_common_model->check_is_visited($ip, 'site_views');
        if (!$is_visited) {
            
            $details = get_location_info($ip);
        // save the ip address and date of one who click the link
            if (isset($details->country)) {
                $lat = 0;
                $long = 0;
                if (isset($details->loc)) {
                    $location = explode(',', $details->loc);
                    if (count($location) > 1) {
                        $lat = $location[0];
                        $long = $location[1];
                    }
                }

                $ipDetails = array(
                    'ip_address' => $ip,
                    'city' => isset($details->city) ? $details->city : '',
                    'region' => isset($details->region) ? $details->region : '',
                    'country' => $details->country,
                    'zipcode' => isset($details->postal) ? $details->postal : '',
                    'latitude' => $lat,
                    'longitude' => $long,
                    'time' => date('Y-m-d H:i:s')
                    );
                $this->admin_common_model->insertRow($ipDetails, 'site_views');
            }
        }

        $this->data['searchtype'] = $this->admin_common_model->getRcTypes();
        $this->data['searchlanguage'] = $this->admin_common_model->getRetreatLanguage();
        $this->gr_template
        ->enable_parser(FALSE)
        ->set_theme('user')
        ->set_layout('default')
        ->title("User's Portal")
        ->set_partial('head')
        ->set_partial('homeslider')
        ->set_partial('header')
        ->set_partial('footerbanner')
        ->set_partial('footer');
        $this->data['theme_path'] = 'themes/user/';
        $currency = $this->session->userdata('country_selected');
//        $ip = gethostbyname('www.google.com');

        $ip = $_SERVER['REMOTE_ADDR'];
//        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        if ($currency) {
            $this->data['currency_id'] = $currency;
        } else {
            if (isset($details->country)) {
                $countries = $this->admin_common_model->getOneWhere(array('iso_alpha2' => $details->country), 'countries');
                $currency_id = $countries->id_countries;
                $country_array = array(98, 227, 30);
                if (!in_array($currency_id, $country_array)) {
                    $currency = 13;
                } else {
                    $currency = $currency_id;
                }
            } else {
                $currency = 98;
            }
            $this->data['currency_id'] = $currency;
        }
        $this->session->set_userdata(array('country' => $this->data['currency_id'], 'country_selected' => $this->data['currency_id']));
    }

}
