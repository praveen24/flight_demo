<?php
define( 'BACKUP_FOLDER', 'Backups');

define( 'SHARE_WITH_GOOGLE_EMAIL', 'jaisonmpaul@gmail.com' );

require_once APPPATH.'third_party/google-api-php-client/src/Google_Client.php';
require_once APPPATH.'third_party/google-api-php-client/src/contrib/Google_DriveService.php';

class Drive {
	
	protected $scope = array('https://www.googleapis.com/auth/drive');
	
	private $_service;
	
	public function __construct( $param ) {
		$client = new Google_Client();
		$client->setClientId( $param['CLIENT_ID'] );
		
		$client->setAssertionCredentials( new Google_AssertionCredentials(
				$param['SERVICE_ACCOUNT_NAME'],
				$this->scope,
				file_get_contents( $param['KEY_PATH'] ) )
		);
		
		$this->_service = new Google_DriveService($client);

		return $this;
	}

	public function initialize($clientId, $serviceAccountName, $key){
		return new Drive($clientId, $serviceAccountName, $key);
	}
	
	public function googleParentReference(){
		return new Google_ParentReference();
	}
	public function __get( $name ) {
		return $this->_service->$name;
	}
	
	public function createFile( $name, $mime, $description, $content, Google_ParentReference $fileParent = null ) {
		$file = new Google_DriveFile();
		$file->setTitle( $name );
		$file->setDescription( $description );
		$file->setMimeType( $mime );
		
		if( $fileParent ) {
			$file->setParents( array( $fileParent ) );
		}
		
		$createdFile = $this->_service->files->insert($file, array(
				'data' => $content,
				'mimeType' => $mime,
		));
		
		return $createdFile['id'];
	}
	
	public function createFileFromPath( $path, $description, Google_ParentReference $fileParent = null ) {
		$fi = new finfo( FILEINFO_MIME );
		$mimeType = explode( ';', $fi->buffer(file_get_contents($path)));
		$fileName = preg_replace('/.*\//', '', $path );
		
		return $this->createFile( $fileName, $mimeType[0], $description, file_get_contents($path), $fileParent );
	}
	
	
	public function createFolder( $name ) {
		return $this->createFile( $name, 'application/vnd.google-apps.folder', null, null);
	}
	
	public function setPermissions( $fileId, $value, $role = 'writer', $type = 'user' ) {
		$perm = new Google_Permission();
		$perm->setValue( $value );
		$perm->setType( $type );
		$perm->setRole( $role );
		
		$this->_service->permissions->insert($fileId, $perm);
	}
	
	public function getFileIdByName( $name ) {
		$files = $this->_service->files->listFiles();
		foreach( $files['items'] as $item ) {
			if( $item['title'] == $name ) {
				return $item['id'];
			}
		}
		
		return false;
	}
	
}