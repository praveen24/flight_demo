<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class pdf {
    
    function __constuct()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
        if ($param == NULL)
        {
            return new mPDF("ml","A4","","",10,10,10,10,6,3);         
        }
         
        return new mPDF($param);
    }
}