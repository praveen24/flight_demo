<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class pdf_ml {
    
    function pdf_ml()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/pdf_ml/mpdf.php';
        if ($params == NULL)
        {
            $param = array("unifont" => array('R' => "unifont.ttf"));         
        }
         
        return new mPDF($param);
    }
}