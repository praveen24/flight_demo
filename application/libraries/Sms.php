<?php
/**
 * File : SMS.php
 * Created by PhpStorm.
 * Project : seedplane
 * Description :
 * User: Vineeth N Krishan
 * Mail: vineeth@soarmorrow.com
 * Date: 8/4/16
 * Time: 11:57 AM
 */
class Sms {


    private static $__sms_url = NULL; // SMS API endpoint
    private static $__access_key = NULL; // SMS Access key
    private static $__credentials = array();

    function __construct($config = array())
    {
        if ( ! empty($config))
        {
            $this->initialize($config);
        }

        log_message('debug', 'SMS Class Initialized');
    }

    // --------------------------------------------------------------------

    /**
     * Initialize preferences
     *
     * @access	public
     * @param	array
     * @return	void
     */
    function initialize($config = array())
    {
        self::$__credentials = $config;

        extract($config);

        if ( ! empty($authkey))
            self::setAuth($authkey);
        if(!empty($sms_url))
            self::setUrl($sms_url);
    }

    /**
     * Set SMS  key and password
     *
     * @param string $accessKey Access key
     * @return void
     */
    public static function setAuth($accessKey)
    {
        self::$__access_key = $accessKey;
    }

    /**
     * @param $url
     */
    public static function setUrl($url){
        self::$__sms_url = $url;
    }

    /**
     * @param $message
     * @param null $numbers
     * @return string
     */
    public static function send($message, $numbers = null){

        $credetials = array_merge(self::$__credentials,array(
            'message' => $message,
            'mobiles' => (is_array($numbers))?implode(',',$numbers):$numbers
        ));
        array_shift($credetials);
        return self::execute_request(http_build_query($credetials));
    }

    /**
     * @param null $postdata
     * @return string
     */
    public static function execute_request($postdata = null)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, self::$__sms_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_encode(['status' => $response]);
    }
}
