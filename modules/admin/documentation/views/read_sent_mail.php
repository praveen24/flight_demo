<section class="content-header">
    <h1>
        Mailbox
        <small>Sent To: <?= $readMail->first_name . ' ' . $readMail->last_name ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="#"><i class="fa fa-envelope"></i> Mailbox</a></li>
        <li class="">Read</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
</section>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Read Mail</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-info">
                            <h3><?= ($readMail->subject) ? ($readMail->subject) : 'No Subject' ?></h3>
                            <h5>Sent to: <?= $readMail->first_name . ' ' . $readMail->last_name ?>
                                <span class="mailbox-read-time pull-right"><?= date('d M Y h:i a', strtotime($readMail->timestamp)) ?></span></h5>
                        </div>
                        <!-- /.mailbox-read-info -->
                        <div class="mailbox-controls with-border text-center">
                            <div class="btn-group">
                                <button type="button" data-link="<?php echo site_url('inbox/delete_sent_mail/' . $readMail->id) ?>" data-id="<?= $readMail->id ?>" class="btn btn-default btn-sm deletemail" data-toggle="tooltip" data-container="body" title="Delete">
                                    <i class="fa fa-trash-o"></i></button>
                                <a href="<?= site_url('inbox/forward/' . $readMail->id) ?>" type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                                    <i class="fa fa-share"></i></a>
                            </div>
                        </div>
                        <!-- /.mailbox-controls -->
                        <div class="mailbox-read-message">
                            <p><?= $readMail->message ?></p>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="<?= site_url('inbox/forward/' . $readMail->id) ?>" type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</a>
                        </div>

                        <button type="button" data-link="<?php echo site_url('inbox/delete_sent_mail/' . $readMail->id) ?>" data-id="<?= $readMail->id ?>" class="btn btn-default btn-sm deletemail" data-toggle="tooltip" data-container="body" title="Delete">
                            <i class="fa fa-trash-o"></i></button>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>
<script type="text/javascript">
    $(window).load(function () {
        $('.deletemail').click(function (e) {
            e.preventDefault();
            var href = $(this).data('link');
            var id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to undo this action!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: href,
                    method: "post",
                    success: function (result) {
                        if (result == true) {
                            swal("Deleted!", "Mail has been deleted.", "success");
                            location.href = '<?= site_url("inbox") ?>';
                        } else {
                            swal("Oops!", "Failed to delete", "error");
                        }
                    }
                });
            });
        });
    });
</script>