<link href="<?= base_url($theme_path . 'plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet"/>

<link href="<?= base_url($theme_path . 'plugins/select2/select2.min.css') ?>" rel="stylesheet" type="text/css" />
<section class="content-header">
    <h1>
        Compose a message
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= site_url('d0cumentation') ?>">chats</a></li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
    <a href="<?= site_url('d0cumentation') ?>" class="btn btn-primary  margin-bottom">Back to d0cumentation</a>
</section>

<section class="content">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box  box-success">
                    <form name="composemessage" method="post" action="" id="chat">
                        <div class="col-md-9">
                            <div class="clearfix"></div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="h5">To: </label>
                                        <?php
                                        if (isset($senderName) && $senderName) {
                                            ?>
                                            <input type="text" class="form-control" readonly name="sender" value="<?= $senderName->first_name . ' ' . $senderName->last_name ?>">
                                            <?php
                                        } else {
                                            ?>
                                            <select name="reciever[]" class="form-control select2" required multiple="" placeholder="To:">
                                                <?php
                                                if (isset($recieverlist)) {
                                                    foreach ($recieverlist as $pr) {
                                                        ?>
                                                        <option value="<?= $pr->id ?>" ><?= $pr->first_name . ' ' . $pr->last_name ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <option value="0" selected >Goretreat Admin</option>
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                            <?php
                                        }
                                        ?>  
                                    </div>
                                    <?php
                                    if (isset($forwardMail) && $forwardMail) {
                                        ?>
                                        <div class="form-group">
                                            <input type="text" name="subject"  readonly class="form-control subject" value="<?= $forwardMail->subject ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control"  name="message" readonly style="height:120px;"><?= $forwardMail->message ?></textarea>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="form-group">
                                            <input type="text" name="subject"  placeholder="Subject" class="form-control subject" value="<?= set_value('subject'); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" name="message" required placeholder="Type the message here" style="height:120px;"><?= set_value('message'); ?></textarea>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer form-group">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Send</button>
                                    </div>
                                    <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /. box -->
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
</section>
</section>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=AIzaSyB3F_CrWPBdKRUWoAHp17SjMadUfUV1T_M"></script>
<script src="<?php base_url($theme_path . 'plugins/geocomplete/jquery.geocomplete.min.js') ?>"></script>

<script src="<?= base_url($theme_path . 'plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>
<?php
if ($this->session->flashdata('message')) {
    ?>
    <script>
        $(window).load(function () {
            console.log("<?= $this->session->flashdata('message') ?>")
            swal({
                title: "<?= $this->session->flashdata('message') ?>",
                text: "",
                type: "success",
                timer: 3000,
                animation: false,
                showConfirmButton: false
            });
        });
    </script>
    <?php
}
?>
<script type="text/javascript">
    $(window).load(function () {
        $(".select2").select2();
    });
</script>