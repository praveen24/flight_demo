<section class="content-header">
    <h1>
        Mailbox
        <small><?= ($sentMessageCount) ? $sentMessageCount : 'No' ?><?= ($sentMessageCount == 1) ? ' sent message' : ' sent messages' ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
    <a href="<?= site_url('inbox') ?>" class="btn btn-primary  margin-bottom">Back to Inbox</a>
</section>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sent Mails</h3>

                        <div class="box-tools pull-right">

                            <div class="has-feedback">
                                <input type="text" name="searchbox" id="searchbox" class="form-control input-sm" placeholder="Search Mail">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-controls">
                            <!-- Check all button -->
                            <a class="btn btn-success" href="<?= site_url('inbox/compose') ?>"><i class="fa fa-edit"></i> Compose</a>
                            <a class="btn btn-default" href="<?= site_url('inbox/sent_messages') ?>"><i class="fa fa-edit"></i> Sent Messages</a>
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 200px">To:</th>
                                        <th>Subject</th>
                                        <th style="width: 175px">Time</th>
                                        <th style="width: 50px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($sentMails as $mail) {
                                        ?>
                                        <tr id="mail<?= $mail->id ?>">
                                            <td class="mailbox-name"><a href="<?= site_url('inbox/read_sent_mail/' . $mail->id) ?>"><?= $mail->first_name . ' ' . $mail->last_name ?></a></td>
                                            <td class="mailbox-subject"><a href="<?= site_url('inbox/read_sent_mail/' . $mail->id) ?>"><b><?= $mail->subject ?></b> - <?= substr($mail->message, 0, 60) ?>...</a>
                                            </td>
                                            <td class="mailbox-date"><?= date('d M Y h:i a', strtotime($mail->timestamp)) ?></td>
                                            <td>
                                                <a href="#" data-link="<?php echo site_url('inbox/delete_sent_mail/' . $mail->id) ?>" data-id="<?= $mail->id ?>" class="deletemail text-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                    <div class="col-md-12">
                        <div class="col-md-10 text-right">
                            <?= $pagination ?>
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>
<?php
if ($this->session->flashdata('message')) {
    ?>
    <script>
        $(window).load(function () {
            console.log("<?= $this->session->flashdata('message') ?>")
            swal({
                title: "<?= $this->session->flashdata('message') ?>",
                text: "",
                type: "success",
                timer: 3000,
                animation: false,
                showConfirmButton: false
            });
        });
    </script>
    <?php
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#searchbox').keyup(function () {
            var valThis = $(this).val().toLowerCase();
            $('.mailbox-messages tr').each(function () {
                var text = $(this).text().toLowerCase();
                (text.indexOf(valThis) == -1) ? $(this).hide() : $(this).show();
            });
        });
    });
    $(window).load(function () {
        $('.deletemail').click(function (e) {
            e.preventDefault();
            var href = $(this).data('link');
            var id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to undo this action!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: href,
                    method: "post",
                    success: function (result) {
                        if (result == true) {
                            $("#mail" + id).remove();
                            swal("Deleted!", "Mail has been deleted.", "success");
                        } else {
                            swal("Oops!", "Failed to delete", "error");
                        }
                    }
                });
            });
        });
    });
</script>