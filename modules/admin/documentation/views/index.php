<section class="content-header">
    <h1>
        Documentation
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Documentation</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
</section>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lists</h3>
                        <form method="GET">
                            <div class="form-group">
                                <label>Select Date Range</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" value="<?= isset($date_range) ? $date_range : '' ?>" name="date_range" readonly="readonly" class="form-control pull-right" id="docdaterange"/>
                                </div><!-- /.input group -->
                            </div>
                            
                            <button type="submit" class="btn btn-primary" id="Search">Search</button>
                        </form>
                        

                        <div class="box-tools pull-right">

                            <div class="has-feedback">
                                <input type="text" name="searchbox" id="searchbox" class="form-control input-sm" placeholder="Search Mail">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-controls">
                            <!-- Check all button -->
                            <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> New Documentation</a>
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Flight No.</th>
                                        <th>Route</th>
                                        <th>Attachment</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($docs as $key => $doc) {
                                        ?>
                                        <tr>
                                            <td><?= (($page - 1) * $per_page + ($key + 1)) ?></td>
                                            <td><?= $doc->type === '1' ? 'FHR' : 'Others' ?></td>
                                            <td><?= date('Y-m-d H:i a', strtotime($doc->date)) ?></td>
                                            <td><?= $doc->flight_no ?></td>
                                            <td><?= $doc->route ?></td>
                                            <td style="width: 90px;height: 90px;">
                                                <?php if(!empty($doc->file)) { ?>
                                                <a href="<?= base_url('uploads/docs/'.$doc->file) ?>">File</a>
                                                <?php } ?>
                                            </td>
                                            <td><?= $doc->remarks ?></td>
                                            <td>
                                                <a href="#" data-id="<?= $doc->id ?>" class="text-danger editmode" data-toggle="modal" data-target="#editModal"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                    <div class="col-md-12">
                        <div class="col-md-10 text-right">
                            <?= $pagination ?>
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Documentation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="content-box-large">
      <div class="panel-body">
        <form class="form-horizontal" role="form" method="post" action="<?= site_url('documentation/add') ?>" enctype="multipart/form-data">

            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Type">Type</label>
                    <select name="type" class="form-control">
                        <option value="0" selected="selected">Select Type</option>
                        <option value="1">FHR</option>
                        <option value="2">Others</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Date">Date</label>
                    <input type="text" name="startdate" placeholder="Click here for selecting date" class="form-control startdate" value="<?= set_value('startdate'); ?>"/>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="flight number">Flight Number</label>
                    <input type="text" name="flight_no" placeholder="Flight Number" class="form-control" value="<?= set_value('flight_no'); ?>"/>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Route">Route</label>
                    <select name="route" class="form-control">
                        <option value="" selected="selected">Select Route</option>
                        <option value="Cochin to Bengaluru">Cochin to Bengaluru</option>
                        <option value="Cochin to Mumbai">Cochin to Mumbai</option>
                        <option value="Cochin to Sharjah">Cochin to Sharjah</option>
                        <option value="Cochin to Kolkata">Cochin to Kolkata</option>
                        <option value="Cochin to Dubai">Cochin to Dubai</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Remarks">Remarks</label>
                    <textarea name="remarks" placeholder="Remarks"  class="form-control"><?= set_value('remarks') ?></textarea>
                </div>
            </div>
            <div class="form-group col-md-6 ">
                <label class="h5" for="exampleInputFile">Add Attachment</label><span>
                <input type="file" class="form-control-file" id="exampleInputFile" name="uploadfile">
            </div>

            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Documentation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="content-box-large">
      <div class="panel-body">
        <form class="form-horizontal" role="form" method="post" action="<?= site_url('documentation/edit') ?>" enctype="multipart/form-data">

            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Type">Type</label>
                    <select name="type" id="typeedit" class="form-control">
                        <option value="0" selected="selected">Select Type</option>
                        <option value="1">FHR</option>
                        <option value="2">Others</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Date">Date</label>
                    <input type="text" name="startdate" id="startdateedit" placeholder="Click here for selecting date" class="form-control startdate" value="<?= set_value('startdate'); ?>"/>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="flight number">Flight Number</label>
                    <input type="text" name="flight_no" id="flight_noedit" placeholder="Flight Number" class="form-control" value="<?= set_value('flight_no'); ?>"/>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Route">Route</label>
                    <select name="route" id="routeedit" class="form-control">
                        <option value="" selected="selected">Select Route</option>
                        <option value="Cochin to Bengaluru">Cochin to Bengaluru</option>
                        <option value="Cochin to Mumbai">Cochin to Mumbai</option>
                        <option value="Cochin to Sharjah">Cochin to Sharjah</option>
                        <option value="Cochin to Kolkata">Cochin to Kolkata</option>
                        <option value="Cochin to Dubai">Cochin to Dubai</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">                                
                <div class="form-group">
                    <label for="Remarks">Remarks</label>
                    <textarea name="remarks" id="remarksedit" placeholder="Remarks"  class="form-control"><?= set_value('remarks') ?></textarea>
                </div>
            </div>
            <div class="form-group col-md-6 ">
            <label>Current file:</label>
                <a id="filename_edit" href=""></a><br>
                <label class="h5" for="exampleInputFile">Add Attachment</label><span>
                <input type="file" class="form-control-file" id="exampleInputFile" name="uploadfile">
            </div>

            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
<?php
if ($this->session->flashdata('message')) {
    ?>
    <script>
        $(window).load(function () {
            swal({
                title: "<?= $this->session->flashdata('message') ?>",
                text: "",
                type: "success",
                timer: 3000,
                animation: false,
                showConfirmButton: false
            });
        });
    </script>
    <?php
}
?>
<style>
    #imagePreview {
        width: 100px;
        max-width: 100px;
        background-position: center center;
        background-size: cover;
    }

    #imagePreview canvas{
        width: 100%;
    }
</style>

<script type="text/javascript">
var base_url = '<?= base_url() ?>';
    $(document).ready(function () {
        $('#searchbox').keyup(function () {
            var valThis = $(this).val().toLowerCase();
            $('.mailbox-messages tr').each(function () {
                var text = $(this).text().toLowerCase();
                (text.indexOf(valThis) == -1) ? $(this).hide() : $(this).show();
            });
        });

        $(".startdate").datepicker({format: 'dd M yyyy', autoclose: true});

        $('#docdaterange').daterangepicker();

        $(".editmode").click(function(e){
            e.preventDefault();
            var that = $(this);
            var id = that.data('id');
            $.ajax({
                    type: 'POST',
                    url: "<?= site_url('documentation/get_docs') ?>",
                    data: {id: id},
                    success: function (data) {
                        var data = JSON.parse(data);
                        console.log(data);
                        $("#typeedit").val(data.type);
                        $("#startdateedit").val(data.date);
                        $("#flight_noedit").val(data.flight_no);
                        $("#routeedit").val(data.route);
                        $("#remarksedit").val(data.remarks);
                        $("#filename_edit").attr({href: base_url+"uploads/docs/" +data.file});
                        $("#filename_edit").html(data.file);
                    }
                });
        });
    });
</script>