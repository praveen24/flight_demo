<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of chats by admins
 *
 * @author Kunhu
 */
class Documentation extends Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('documentation_model');
        $this->load->library('pagination');
    }

    function index() {
        $filters = array();
        $this->data['date_range'] = $this->input->get('date_range');
            $range = explode('-', $this->input->get('date_range'));
            $start = 0;
            $end = 0;
            if (isset($range)) {
                if (isset($range[0]) && $range[0] != '') {
                    $start = date(DATE_FORMAT, strtotime($range[0]));
                }
                if (isset($range[1]) && $range[1] != '') {
                    $range[1] = str_replace('?', '', $range[1]);
                    $end = date(DATE_FORMAT, strtotime($range[1] . " + 1 DAY"));
            }
        }
        $filters['from_date'] = $this->data['from_date'] = $start;
        $filters['to_date'] = $this->data['to_date'] = $end;
        $filters['searchbox'] = $this->input->post('searchbox');
        $total = $this->documentation_model->getDocsCount($filters);
        //pagination settings
        $config['base_url'] = site_url('documentation/index');
        $config['total_rows'] = $total;
        $config['per_page'] = 5;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $config['use_page_numbers'] = TRUE;
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        $this->data['per_page'] = $config['per_page'];
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['filters'] = $filters;
        $this->data['docs'] = $this->documentation_model->getDocs($this->data['per_page'], $this->data['page'], $filters);
        $this->gr_template->build('index', $this->data);
    }

    function add(){
        $this->load->library('upload');
        $image['file_name'] = '';
        if ($this->input->post()) {
            if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                $file = $_FILES["uploadfile"]['tmp_name'];
                $path = './uploads/docs/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, TRUE);
                }
                $config = $this->set_upload_options($path);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('uploadfile')) {
                } else {
                    $image = $this->upload->data('file_name');
                }
            }
            $details = array(
                'type' => $this->input->post('type'),
                'date' => date('Y-m-d H:i', strtotime($this->input->post('startdate'))),
                'flight_no' => $this->input->post('flight_no'),
                'route' => $this->input->post('route'),
                'remarks' => $this->input->post('remarks'),
                'file' => $image['file_name']
                );
            $this->documentation_model->insertRow($details, 'documentation');
            $this->session->set_flashdata('message', 'Documentation Added successfully');
            $this->data['typealert'] = "success";
            $this->data['message'] = "Documentation Added";
            redirect(site_url('documentation'));
        }
    }

    function set_upload_options($path) {
        // upload an image options
        $config = array();
        $config['upload_path'] = $path; //give the path to upload the image in folder
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE; // for encrypting the name
        $config['allowed_types'] = '*';
        $config['max_size'] = '78000';
        $config['overwrite'] = FALSE;
        return $config;
    }

    function edit(){
        $this->load->library('upload');
        if ($this->input->post()) {
            if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                $file = $_FILES["uploadfile"]['tmp_name'];
                $path = './uploads/docs/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, TRUE);
                }
                $config = $this->set_upload_options($path);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('uploadfile')) {
                } else {
                    $image = $this->upload->data('file_name');
                }
            }
            $details = array(
                'type' => $this->input->post('type'),
                'date' => date('Y-m-d H:i', strtotime($this->input->post('startdate'))),
                'flight_no' => $this->input->post('flight_no'),
                'route' => $this->input->post('route'),
                'remarks' => $this->input->post('remarks'),
                'file' => $image['file_name'],
                'status' => 2
                );
            $this->documentation_model->insertRow($details, 'documentation');
            $this->session->set_flashdata('message', 'Documentation Edited successfully');
            $this->data['typealert'] = "success";
            $this->data['message'] = "Documentation Edited";
            redirect(site_url('documentation'));
        }
    }

    public function get_docs(){
        $doc = $this->documentation_model->getOneWhere(array('id' => $this->input->post('id')), 'documentation');
        echo json_encode($doc);
    }

}
