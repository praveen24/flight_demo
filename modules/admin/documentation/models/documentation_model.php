<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of chat_model
 *
 * @author soarmorrow
 */
class Documentation_model extends MY_Model {

    //put your code here 

    public function __construct() {
        parent::__construct();
    }

    public function getAllAdmins($user, $role, $selfrole) {
        return $this->db->select('u.*')
                        ->from('user u')
                        ->join('user_role ur', 'u.id = ur.user_id')
                        ->where('ur.role_id', $role)
                        ->or_where('ur.role_id', $selfrole)
                        ->where('ur.user_id !=', $user)
                        ->get()
                        ->result();
    }

    function updateStatus($id, $array, $table) {
        $this->db->where('id', $id)
                ->update('chats', $array);
    }

    function getDocs($per_page, $page, $filters) {
        if (isset($per_page)) {
            $this->db->limit($per_page, (($page - 1) * $per_page));
        }
        if (isset($filters['searchbox']) && $filters['searchbox'] != "") {
            $this->db->where("(`type` LIKE '%" . $filters['searchbox'] . "%' OR `route` LIKE '%" . $filters['searchbox'] . "%' OR `flight_no` LIKE '%" . $filters['searchbox'] . "%' OR `remarks` LIKE '%" . $filters['searchbox'] . "%')");
        }
        if ($filters['from_date'] != '') {
            $this->db->where("date >= '" . $filters['from_date'] . "'");
        }
        if ($filters['to_date'] != '') {
            $this->db->where("date <= '" . $filters['to_date'] . "'");
        }
        $this->db->select('*')
                ->from('documentation');
        return $this->db->order_by('id', 'desc')
                        ->get()
                        ->result();
    }

    // function getNewMessageCount($user_id, $is_gr_admin = false){
    //    $this->db->select('c.*')
    //    ->from('chats c')
    //    ->join('user u', 'u.id = c.sender_id')
    //    ->where('c.reciever_id', $user_id);
    //    if ($is_gr_admin) {
    //     $this->db->or_where('c.reciever_id', 0);
    // }
    // return $this->db->where('c.inbox_deleted !=', 1)
    // ->where('c.status', 0)
    // ->get()
    // ->num_rows();
// }

    function getDocsCount($filters) {
        if ($filters['from_date'] != '') {
            $this->db->where("date >= '" . $filters['from_date'] . "'");
        }
        if ($filters['to_date'] != '') {
            $this->db->where("date <= '" . $filters['to_date'] . "'");
        }
        return $this->db->select('*')
                ->from('documentation')
                ->get()
                ->num_rows();
    }

    function deleteInboxMail($id, $array, $table) {
        return $this->db->where('id', $id)
                        ->update('chats', $array);
    }

    function readMail($id) {
        return $this->db->select('c.*, u.first_name, u.last_name, u.id as user_id')
                        ->from('chats c')
                        ->join('user u', 'u.id = c.sender_id')
                        ->where('c.id', $id)
                        ->get()
                        ->row();
    }

    function readSentMail($id) {
        return $this->db->select('c.*, u.first_name, u.last_name, u.id as user_id')
                        ->from('chats c')
                        ->join('user u', 'u.id = c.reciever_id')
                        ->where('c.id', $id)
                        ->get()
                        ->row();
    }

    function getSentMessageCount($user_id) {
        return $this->db->select('c.*')
                        ->from('chats c')
                        ->join('user u', 'u.id = c.sender_id')
                        ->where('c.sender_id', $user_id)
                        ->where('c.is_sent_deleted', 0)
                        ->get()
                        ->num_rows();
    }

    function getSentMailCount($filters, $user_id) {
        return $this->db->select('c.*')
                        ->from('chats c')
                        ->join('user u', 'u.id = c.sender_id')
                        ->where('c.sender_id', $user_id)
                        ->get()
                        ->num_rows();
    }

    function getSentMails($user_id, $per_page, $page, $filters) {
        if (isset($per_page)) {
            $this->db->limit($per_page, (($page - 1) * $per_page));
        }
        if (isset($filters['searchbox']) && $filters['searchbox'] != "") {
            $this->db->where("(`subject` LIKE '%" . $filters['searchbox'] . "%' OR `message` LIKE '%" . $filters['searchbox'] . "%')");
        }
        return $this->db->select('c.*, u.first_name, u.last_name')
                        ->from('chats c')
                        ->join('user u', 'u.id = c.sender_id')
                        ->where('c.sender_id', $user_id)
                        ->where('c.is_sent_deleted !=', 1)
                        ->order_by('c.timestamp', 'desc')
                        ->get()
                        ->result();
    }

    function deleteSentMail($id, $array, $table) {
        return $this->db->where('id', $id)
                        ->update('chats', $array);
    }

    function getGRAdminList() {
        return $this->db->select('u.id')
                        ->from('user u')
                        ->join('user_role ur', 'ur.user_id = u.id')
                        ->where('ur.role_id', 1)
                        ->get()
                        ->result();
    }

}
