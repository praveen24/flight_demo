<section class="content-header">
    <h1>
        Report Details
    </h1><br />
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <?php
    if (_is("RC Admin")) {
        ?>
        <div class="row">
            <div class="col-md-6">
                <h3>Bookings Based on Countries</h3>
                <div id="world-map" style="width: 600px; height: 400px"></div>
            </div>
            <div class=" col-md-6 table-responsive">

                <table id="listevents" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Last Three Events</th>
                            <th>Total Seats</th>
                            <th>Booked Seats</th>  
                            <th>Remaining Seats</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($last3events as $value) {
                            ?>
                            <tr id="centre<?= $value->id ?>">
                                <td><a href="<?= site_url('event_public/single_event/' . $value->id) ?>"> <?= $value->name ?></a></td>
                                <td><?= $value->available_seats ?></td>
                                <td><?= $value->booked_seats ?></td>
                                <td><?= $value->available_seats - $value->booked_seats ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $new_bookings ?></h3>
                    <p>New Bookings</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= intval($increase_percent) ?><sup style="font-size: 20px">%</sup></h3>
                    <p>Increase in Visitors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $new_users ?></h3>
                    <p>New Visitors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $unique_visitors ?></h3>
                    <p>Unique Visitors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->

            <!--/.nav-tabs-custom--> 

            <!-- solid sales graph -->
            <div class="box box-solid bg-teal-gradient">
                <div class="box-header">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Booking Graph</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <div class="chart" id="line-chart" style="height: 250px;"></div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </section><!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

            <!-- Map box -->
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Visitors</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <div class="chart" id="revenue-chart" style="height: 250px;"></div>
                </div><!-- /.box-body -->

            </div><!-- /.box -->

            <!-- Map box -->
        </section><!-- right col -->


        <section class="col-sm-12 connectedSortable">

            <!-- Map box -->
            <div class="box box-solid bg-purple-gradient">
                <div class="box-header">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Cash flow</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <div class="chart" id="booking-chart" style="height: 250px;"></div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <!-- Map box -->
        </section><!-- right col -->
    </div><!-- /.row (main row) -->
    <?php
    if (_is('GR Admin') || _is('RC Admin')) {
        $heading = 'Views for the WEBSITE';
        ?>
        <div class="box box-solid bg-teal-gradient">
            <div class="box-header">
              <h3 class="box-title text-center"><?= _is('GR Admin') ? 'Views for the WEBSITE' : 'Views for the Retreat Center' ?></h3>
          </div>
          <div class="">
            <div class="btn-group">
              <button type="button" id="day" class="btn btn-default">Today</button>
              <button type="button" id="week" class="btn btn-default">This Week</button>
              <button type="button" id="month" class="btn btn-default">This Month</button>
          </div>
          <div id="day-view-map" style="display: none;" style="width: 100%"></div>
          <div id="week-view-map" style="display: none;" style="width: 100%"></div>
          <div id="month-view-map" style="display: none;" style="width: 100%"></div>
      </div>
  </div>
  <?php
}
?>

<script>
    RCdashBoardinit();
</script>
</section>