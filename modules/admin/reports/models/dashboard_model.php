<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends MY_Model {

    public function __construct($table = null) {
        parent::__construct();
    }

    public function get_centername($center) {
        return $this->db->select('*')
                        ->from('center')
                        ->where('id', $center)
                        ->get()
                        ->row();
    }

    public function get_new_bookings($center = NULL) {
        if ($center != NULL) {
            $this->db->where('E.center_id', $center);
        }
        $row = $this->db->select('SUM(EO.attend) as count', FALSE)
                ->from('event_orders as EO')
                ->join('events as E', 'E.id=EO.event_id')
                ->where("EO.timestamp >= ", date('Y-m-d', strtotime('-30 days')))
                ->get()
                ->row();
        return $row->count ? $row->count : 0;
    }

    public function get_unique_visitors($center) {
        return $this->db->query("SELECT count(V.ip_address) as count FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE ((V.type_id=1 AND E.center_id=$center) OR (V.type_id=2 AND V.item_id=$center)) AND created_at >= '" . date('Y-m-d H:i:s', strtotime('-30 days')) . "' GROUP BY V.ip_address ")->num_rows();
    }

    public function get_all_unique_visitors() {
        return $this->db->query("SELECT count(V.ip_address) as count FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE  created_at >= '" . date('Y-m-d H:i:s', strtotime('-30 days')) . "' GROUP BY V.ip_address ")->num_rows();
    }

    public function get_new_visitors($center) {
        $row = $this->db->query("SELECT COUNT(*) as count FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE ((V.type_id=1 AND E.center_id=$center) OR (V.type_id=2 AND V.item_id=$center)) AND created_at >= '" . date('Y-m-d H:i:s', strtotime('-30 days')) . "'")->row();
        return $row->count ? $row->count : 0;
    }

    public function get_all_visitors() {
        $row = $this->db->query("SELECT COUNT(*) as count FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE created_at >= '" . date('Y-m-d H:i:s', strtotime('-30 days')) . "'")->row();
        return $row->count ? $row->count : 0;
    }

    public function get_old_visitors($center) {
        $row = $this->db->query("SELECT COUNT(*) as count FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE ((V.type_id=1 AND E.center_id=$center) OR (V.type_id=2 AND V.item_id=$center)) AND created_at >= '" . date('Y-m-d H:i:s', strtotime('-60 days')) . "'")->row();
        return $row->count ? $row->count : 0;
    }

    public function get_all_old_visitors() {
        $row = $this->db->query("SELECT COUNT(*) as count FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE  created_at >= '" . date('Y-m-d H:i:s', strtotime('-60 days')) . "'")->row();
        return $row->count ? $row->count : 0;
    }

    public function get_visitors_by_month($center) {
        return $this->db->query("SELECT MONTHNAME(created_at) as y1 ,DATE_FORMAT(created_at,'%Y-%m') AS y,COUNT(*) as item1 FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . "WHERE ((V.type_id=1 AND E.center_id=$center) OR (V.type_id=2 AND V.item_id=$center)) GROUP BY y1 ")->result_array();
    }

    public function get_bookings_ByMonth($center = NULL) {
        if ($center != NULL) {
            $this->db->where('E.center_id', $center);
        }
        $result_array = $this->db->select("SUM(EO.attend) as count,DATE_FORMAT(EO.timestamp,'%Y-%m') AS y", FALSE)
                        ->from('event_orders as EO')
                        ->join('events as E', 'E.id=EO.event_id')
                        ->group_by("MONTH(EO.timestamp),YEAR(EO.timestamp)")
                        ->get()->result_array();
        return $result_array;
    }

    public function get_bookings_ByLocation() {
        $result = $this->db->select("SUM(location_based_booking.seats) as sum, country_code")
                ->from('location_based_booking')
                ->group_by('country_code')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country_code] = $value->sum;
            }
        }

        return $return;
    }

    public function day_site_hits(){
        $result = $this->db->select("COUNT(ip_address) as count, country")
                ->from('site_views')
                ->where("DATE(time) = '".date('Y-m-d')."'", NULL, FALSE)
                ->group_by('country')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country] = $value->count;
            }
        }
        return $return;
    }

    public function week_site_hits(){
        $result = $this->db->select("COUNT(ip_address) as count, country")
                ->from('site_views')
                ->where("time >= '".date('Y-m-d')."' - INTERVAL 7 DAY", NULL, FALSE)
                ->group_by('country')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country] = $value->count;
            }
        }
        return $return;
    }

    public function month_site_hits(){
        $result = $this->db->select("COUNT(ip_address) as count, country")
                ->from('site_views')
                ->where("time LIKE '%".date('Y-m-')."%'", NULL, FALSE)
                ->group_by('country')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country] = $value->count;
            }
        }
        return $return;
    }

    public function day_center_hits($center_id){
        $result = $this->db->select("COUNT(ip_address) as count, country")
                ->from('center_views')
                ->where('center_id', $center_id)
                ->where("DATE(time) = '".date('Y-m-d')."'", NULL, FALSE)
                ->group_by('country')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country] = $value->count;
            }
        }
        return $return;
    }

    public function week_center_hits($center_id){
        $result = $this->db->select("COUNT(ip_address) as count, country")
                ->from('center_views')
                ->where('center_id', $center_id)
                ->where("time >= '".date('Y-m-d')."' - INTERVAL 7 DAY", NULL, FALSE)
                ->group_by('country')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country] = $value->count;
            }
        }
        return $return;
    }

    public function month_center_hits($center_id){
        $result = $this->db->select("COUNT(ip_address) as count, country")
                ->from('center_views')
                ->where('center_id', $center_id)
                ->where("time LIKE '%".date('Y-m-')."%'", NULL, FALSE)
                ->group_by('country')
                ->get()
                ->result();
        $return = array();
        if ($result) {
            foreach ($result as $key => $value) {
                $return[$value->country] = $value->count;
            }
        }
        return $return;
    }

    public function get_all_visitors_by_month() {
        return $this->db->query("SELECT MONTHNAME(created_at) as y1 ,DATE_FORMAT(created_at,'%Y-%m') AS y,COUNT(*) as item1 FROM views as V "
                        . "LEFT JOIN events as E ON E.id=V.item_id "
                        . " GROUP BY y1 ")->result_array();
    }

    function get_all_bookings_by_month() {
        $dtime = $this->db->query("SELECT DATE_FORMAT(timestamp,'%Y-%m') AS dtime FROM event_orders GROUP BY dtime")->result();
        $array = array();
        foreach ($dtime as $value) {
            $INR = $this->db->select('SUM(amount) AS sum,SUM(gr_commission) AS gr_sum,SUM(rc_commission) AS rc_sum')
                            ->where("DATE_FORMAT(timestamp,'%Y-%m')", $value->dtime)
                            ->where('currency', 'INR')
                            ->get('event_orders')->row();

            $USD = $this->db->select('SUM(amount) AS sum,SUM(gr_commission) AS gr_sum,SUM(rc_commission) AS rc_sum')
                            ->where("DATE_FORMAT(timestamp,'%Y-%m')", $value->dtime)
                            ->where('currency', 'USD')
                            ->get('event_orders')->row();
            $array[] = array('INR' => ($INR->sum) ? $INR->sum : 0, 'USD' => ($USD->sum) ? $USD->sum : 0, 'y' => $value->dtime);
        }
        return $array;
    }

    function get_bookings_by_month($center = null) {

        if ($center != NULL) {
            $dtime = $this->db->query("SELECT DATE_FORMAT(EO.timestamp,'%Y-%m') AS dtime FROM event_orders AS EO LEFT JOIN events as E ON E.id=EO.event_id WHERE E.center_id=$center GROUP BY dtime")->result();
        } else {
            $dtime = $this->db->query("SELECT DATE_FORMAT(EO.timestamp,'%Y-%m') AS dtime FROM event_orders AS EO LEFT JOIN events as E ON E.id=EO.event_id GROUP BY dtime")->result();
        }
        $array = array();
        foreach ($dtime as $value) {
            if ($center != NULL) {
                $this->db->where('E.center_id', $center);
            }
            $INR = $this->db->select('SUM(amount) AS sum,SUM(gr_commission) AS gr_sum,SUM(rc_commission) AS rc_sum')
                            ->where("DATE_FORMAT(timestamp,'%Y-%m')", $value->dtime)
                            ->where('currency', 'INR')
                            ->join('events AS E', 'E.id=event_orders.event_id')
                            ->get('event_orders')->row();
            if ($center != NULL) {
                $this->db->where('center_id', $center);
            }
            $USD = $this->db->select('SUM(amount) AS sum,SUM(gr_commission) AS gr_sum,SUM(rc_commission) AS rc_sum')
                            ->where("DATE_FORMAT(timestamp,'%Y-%m')", $value->dtime)
                            ->where('currency', 'USD')
                            ->join('events AS E', 'E.id=event_orders.event_id')
                            ->get('event_orders')->row();
            if ($center != NULL) {
                $array[] = array('INR' => ($INR->sum) ? $INR->rc_sum : 0, 'USD' => ($USD->rc_sum) ? $USD->sum : 0, 'y' => $value->dtime);
            } else {
                $array[] = array('INR' => ($INR->sum) ? $INR->sum : 0, 'USD' => ($USD->sum) ? $USD->sum : 0, 'y' => $value->dtime);
            }
        }
        return $array;
    }

    public function checkVerified($center) {
        return $this->db->select('verified')
                        ->from('center')
                        ->where('id', $center)
                        ->get()
                        ->row()->verified;
    }

    public function getPendingVerification() {
        return $this->db->select('verified')
                        ->from('center')
                        ->where('verified', 0)
                        ->where('is_deleted', 0)
                        ->get()
                        ->row();
    }

    public function getPendingBankDetailsApproval(){
        return $this->db->select('bd.*, c.name as centername')
                        ->from('bank_details bd')
                        ->join('center c', 'bd.center_id = c.id', 'LEFT')
                        ->where('bd.pending_approval', 1)
                        ->order_by('created_at', 'DESC')
                        ->get()
                        ->result();
    }

    public function getLatestEvents($id) {
        $data = $this->db->select('*')
                ->from('events')
                ->where('center_id', $id)
                ->where('published', 1)
                ->where('start_date > NOW()')
                ->order_by('start_date', 'ASC')
                ->limit(3)
                ->get()
                ->result();
        $return = array();
        foreach ($data as $value) {
            $res = $this->db->select('SUM(attend) AS attend', TRUE)
                            ->from('event_orders')
                            ->where('event_id', $value->id)
                            ->get()->row();
            $value->booked_seats = ($res->attend) ? $res->attend : 0;
            $return[] = $value;
        }
        return $return;
    }
}
