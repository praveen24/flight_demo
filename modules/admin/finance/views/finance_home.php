<style type="text/css">
    .select2{
        width: 100% !important;
    }
</style>

<link href="<?= base_url($theme_path . 'plugins/ionslider/ion.rangeSlider.css') ?>" rel="stylesheet" type="text/css" />
<!-- ion slider Nice -->
<link href="<?= base_url($theme_path . 'plugins/ionslider/ion.rangeSlider.skinNice.css') ?>" rel="stylesheet" type="text/css" />
<!-- bootstrap slider -->
<link href="<?= base_url($theme_path . 'plugins/bootstrap-slider/slider.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url($theme_path . 'plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet"/>
<!-- Select2 -->
<link href="<?= base_url($theme_path . 'plugins/select2/select2.min.css') ?>" rel="stylesheet" type="text/css" />
<section class="content-header">
    <h1>
        Center Analytics
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= site_url('centre') ?>">Centre</a></li>
        <li class="active">Center Analytics</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
</section>
<section class="content">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <form name="searchform" method="GET" action="" id="formSearch">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="h5">Country</label>
                                    <select name="country" class="form-control select2 country_value">
                                        <option value="">All Countries</option>
                                        <?php
                                        foreach ($countries as $value) {
                                            if ($value->country == $country) {
                                                echo '<option value="' . $value->country . '" selected>' . $value->country . '</option>';
                                            } else {
                                                echo '<option value="' . $value->country . '">' . $value->country . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class="h5">State</label>
                                    <select name="state" class="form-control select2" id="state_value">
                                        <option value="">All States</option>
                                        <?php
                                        foreach ($states as $value) {
                                            if ($value->state == $state) {
                                                echo '<option value="' . $value->state . '" selected>' . $value->state . '</option>';
                                            } else {
                                                echo '<option value="' . $value->state . '">' . $value->state . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class="h5">City</label>
                                    <select name="city" class="form-control select2" id="city_value">
                                        <option value="">All Cities</option>

                                        <?php
                                        foreach ($cities as $value) {
                                            if ($value->city == $city) {
                                                echo '<option value="' . $value->city . '" selected>' . $value->city . '</option>';
                                            } else {
                                                echo '<option value="' . $value->city . '">' . $value->city . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-3">
                                    <label class="h5">Category</label>
                                    <select name="rc_cat" class="form-control select2">
                                        <option value="">All Categories</option>
                                        <?php
                                        if (isset($rccats)) {
                                            foreach ($rccats as $cat) {
                                                if ($cat->id == $rc_cat) {
                                                    ?>
                                                    <option selected value="<?= $cat->id ?>" <?php if ((isset($filters['rc_cat']) ? $filters['rc_cat'] : "") == $cat->id) echo 'selected'; ?>><?= $cat->rc_category ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?= $cat->id ?>" <?php if ((isset($filters['rc_cat']) ? $filters['rc_cat'] : "") == $cat->id) echo 'selected'; ?>><?= $cat->rc_category ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class="h5">Type</label>
                                    <select name="rc_type" class="form-control select2">
                                        <option value="">All types</option>
                                        <?php
                                        if (isset($rctypes)) {
                                            foreach ($rctypes as $type) {
                                                if ($type->id == $rc_type) {
                                                    ?>
                                                    <option selected value="<?= $type->id ?>" <?php if ((isset($filters['rc_type']) ? $filters['rc_type'] : "") == $type->id) echo 'selected'; ?>><?= $type->name ?></option>
                                                    <?php
                                                }else {
                                                    ?>
                                                    <option value="<?= $type->id ?>" <?php if ((isset($filters['rc_type']) ? $filters['rc_type'] : "") == $type->id) echo 'selected'; ?>><?= $type->name ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select> 
                                </div>

                                <div class="col-sm-6">
                                    <label class="h5">Select Seat Range</label>
                                    <input id="range_1" type="text" name="seat_range" value="0;<?= isset($maxseats->total_seats) ? $maxseats->total_seats : 1 ?>" data-from="<?= (isset($seat_range[0])) ? $seat_range[0] : "" ?>" data-to="<?= (isset($seat_range[1])) ? $seat_range[1] : "" ?>"/>
                                </div>
                            </div>
                            <div class="row printDate" >
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Select Booking Range</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" value="<?= isset($date_range) ? $date_range : '' ?>" name="date_range" readonly="readonly" class="form-control pull-right" id="reservation"/>
                                        </div><!-- /.input group -->
                                    </div><!-- /.form group -->
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" id="Search">Search</button>
                        </div>
                    </form>
                </div><!-- /.box -->
                <div class="box box-success">
                    <div class="box-body">    
                        <div class="dropdown pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary">Download</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <!--<span class="sr-only">Toggle Dropdown</span>-->
                                </button>
                                <?php
                                $myuri = $_SERVER['REQUEST_URI'];
                                if (strpos('?', $myuri) == 0) {
                                    $myuri = $myuri . '?';
                                }
                                ?>
                                <ul class="dropdown-menu" role="menu">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $myuri . "&loadtype=excel" ?>">Excel File</a></li>
                                    <!-- <li role="presentation"><a role="menuitem" tabindex="-1"  href="<?= $myuri . "&loadtype=csv" ?>">Csv File</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class=" col-md-12 table-responsive">
                            <table id="listanalytics" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="">Name</th>
                                        <th  style="width: 150px">Address</th>  
                                        <th>No of Events</th>
                                        <th>Total Seats</th> 
                                        <th>Bookings</th>
                                        <th style="width: 130px">Total Fund From Razorpay (INR)</th>
                                        <th>Type</th>
                                        <th>Category</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($analytics as $value) {
                                        ?>
                                        <tr>
                                            <td style=""><?= $value->center_name ?></td>
                                            <td><?= $value->street_address1 . " " . $value->street_address2 . " " . $value->city . ' ' . $value->state . " " . $value->zipcode . " " . $value->country ?></td>
                                            <td><?= $value->event_count ?></td>
                                            <td><?= $value->total_seats ?></td> 
                                            <td>
                                                <?php
                                                $sum = 0;
                                                foreach ($value->booked as $value1) {
                                                    $sum = $sum + $value1['booked'];
                                                }
                                                echo $sum;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $sum = 0;
                                                foreach ($value->booked as $value1) {
                                                    if ($value1['currency'] == 'INR')
                                                        $sum = $sum + $value1['fund'];
                                                }
                                                echo $sum;
                                                ?>
                                            </td>
                                            <td><?= $value->type_name ?></td>
                                            <td><?= $value->rc_name ?></td>
                                            <td>
                                                <a href="<?= site_url('booking?key=&center_id=' . $value->center_id) ?>" class="btn btn-sm btn-primary" title="view">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section>
</section>
<style>
    .btn i{
        height: 10px;
    }
    .table > tbody > tr > td, .table > tfoot > tr > td,  .table > thead > tr > td{
        vertical-align: middle;
    }
</style>

<!-- Ion Slider -->
<script src="<?= base_url($theme_path . 'plugins/ionslider/ion.rangeSlider.min.js') ?>" type="text/javascript"></script>

<!-- Bootstrap slider -->
<script src="<?= base_url($theme_path . 'plugins/bootstrap-slider/bootstrap-slider.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url($theme_path . 'plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url($theme_path . 'plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>

<!-- Select2 -->
<script src="<?= base_url($theme_path . 'plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>

<script>
    function formatDate(date) {
        if (date != null) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
        } else {
            var d = new Date(),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
        }
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }
    $(function () {
        $('.loader').css('height', $(window).height());
        createChart = function (id) {
            var timerange = $("#reservation").val();
            var res = timerange.split("-");
            var start = new Date(res[0]);
            var end = new Date(res[1]);
            var ref = "<?= site_url('analytics/getgraphdata'); ?>" + '/' + start.getTime() + '/' + end.getTime();
            $.getJSON(ref, function (data) {
                // Create the chart
                var darray = [];
                $.each(data, function (j, quotedata) {
                    var d = new Date(quotedata.Date);
                    darray[j] = [d.getTime(), Math.round(parseFloat(quotedata.Bookings) * 100) / 100];
                });
                $('#' + id).highcharts('StockChart', {
                    rangeSelector: {
                        selected: 5
                    },
                    title: {
                        text: 'Center Bookings'
                    },
                    series: [{
                            name: "Bookings",
                            data: darray.sort(),
                            tooltip: {
                                valueDecimals: 0
                            }
                        }]
                });
                $('.highcharts-range-selector').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                    onSelect: function ()
                    {
                        // The "this" keyword refers to the input (in this case: #someinput)
                        this.focus();
                    }
                });
                $('.loader').hide();
            });
        }
        createChart('container', '');
    });
    $(window).load(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        $("#range_1").ionRangeSlider({
            type: 'double',
            onChange: function (obj) {
            }
        });

        //Date range picker
        $('#reservation').daterangepicker();

        $("#listanalytics").dataTable({
            "order": [[0, "asc"]],
            "bInfo": false,
            "columnDefs": [
                {"orderable": false, "targets": [8]}
            ],
            searching: false,
            paging: true
        });
        $(".country_value").change(function () {
            loadstates($(this).val());
            loadcities('');
        });
        function loadstates(val) {
            $('#state_value').html("Loading..");
            $.ajax({
                url: "<?= site_url('analytics/change_country') ?>/" + val + '',
                method: "post",
                success: function (data) {
                    $('#state_value').html(data);
                    $("#state_value").select2("destroy");
                    $('#state_value').select2();
                }
            });
        }
        function loadcities(val) {
            $('#city_value').html("Loading..");
            $.ajax({
                url: "<?= site_url('analytics/change_state') ?>/" + val + '',
                method: "post",
                success: function (data) {
                    $('#city_value').html(data);
                    $("#city_value").select2("destroy");
                    $('#city_value').select2();
                }
            });
        }
        $("#state_value").change(function () {
            loadcities($(this).val());
        });
        $('.statuschanger').change(function () {
            var value1;
            if ($(this).prop('checked')) {
                value1 = 1;
            } else {
                value1 = 0;
            }
            var variablee = $(this);
            $.ajax({
                url: "<?= site_url('centre/verify') ?>/" + $(this).val() + '/' + value1,
                method: "post",
                success: function (result) {

                    if (result === '1') {
                        swal("Success!", "Centre status has been changed.", "success");
                    } else {
                        if (variablee.prop('checked')) {
                            variablee.removeAttr('checked');
                        } else {
                            variablee.prop('checked');
                        }
                    }
                }
            });
        });
        $("#listuser").dataTable({
            order: [[1, 'asc']],
            "columnDefs": [
                {"orderable": false, "targets": [0, 8]}
            ]
        });

        $('.deletecentre').click(function () {
            var href = $(this).data('link');
            var id = $(this).data('id');
            var name = $(this).data("namevalue");
            swal({
                title: "Are you sure?",
                text: "You will not be able to undo this action!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: href,
                    method: "post",
                    success: function (result) {
                        if (result === '1') {
                            $("#centre" + id).remove();
                            swal("Deleted!", "Centre '" + name + "' has been deleted.", "success");
                        } else {
                            swal("Oops!", "Failed to delete", "error");
                        }
                    }
                });
            });
        });
    });
</script>
<script src="<?= base_url($theme_path . 'bootstrap/js/highstock.js') ?>" type="text/javascript"></script>
<script src="<?= base_url($theme_path . 'bootstrap/js/exporting.js') ?>" type="text/javascript"></script><!-- Sparkline -->
