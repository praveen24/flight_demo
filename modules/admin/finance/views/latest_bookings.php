<section class="content-header">
    <h1>
        Latest Bookings
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Latest Bookings</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form name="searchform" method="post" action="<?= site_url('finance/latest_bookings') ?>">
                    <div class="box-header">
                        <h3 class="box-title">Search</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="input-group">
                            <input type="text" name="content" placeholder="Enter search key customer name or event name" class="form-control" value="<?= (isset($content) ? $content : "") ?>"/>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">

                    </div>
                </form>
            </div><!-- /.box -->

            <div class="box box-success">
                <div class="box-body">    

                    <?php
                    if($latest_bookings){
                    ?>
                    <div class=" col-md-12 table-responsive">
                        <table id="listcentres" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 125px">Name</th>
                                    <th style="width: 125px">Email</th>
                                    <th style="width: 125px">Center</th>
                                    <th style="width: 125px">Event</th>
                                    <th style="width: 125px">Start Date</th>
                                    <th style="width: 125px">End Date</th>
                                    <th style="width: 125px">Amount</th>
                                    <th style="width: 125px">Booking Date</th>
                                    </tr>
                                </thead>
                                <tbody class="reorder_blog">

                                <?php
                                foreach ($latest_bookings as $value) {
                                ?>
                                <tr>
                                <td><?= $value->toname ?></td>
                                <td><?= $value->email ?></td>
                                <td><?= $value->centername ?></td>
                                <td><?= $value->eventname ?></td>
                                <td><?= date('Y-m-d', strtotime($value->start_date)).date(' h:i A',strtotime($value->start_time)) ?></td>
                                <td><?= date('Y-m-d', strtotime($value->end_date)).date(' h:i A',strtotime($value->end_time)) ?></td>
                                <td><?= $value->amount ?></td>
                                <td><?= $value->timestamp ?></td>
                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">   
                        <div class="col-md-2" style="margin: 20px auto;">
                            <select class="form-control" id="perpage" style="max-width:80px;">
                                <?php
                                if (isset($perpages)) {
                                    foreach ($perpages as $perpage) {
                                        if ($perpage === $per_page) {
                                            echo '<option value="' . $perpage . '" selected>' . $perpage . '</option>';
                                        } else {
                                            echo '<option value="' . $perpage . '">' . $perpage . '</option>';
                                        }
                                    }
                                }
                                ?>                                            
                            </select>
                        </div>
                        <div class="col-md-10 text-right">
                            <?= $pagination ?>
                        </div>
                    </div>
                    <?php
                }
                else{
                    echo "No Datas Found Here";
                }
                    ?>
                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div>

    </div><!-- /.row -->
    <script>
        $(window).load(function () {
            $("#perpage").change(function () {
                $.ajax({
                    url: "<?= site_url('finance/change_perpage') ?>/" + $(this).val() + '',
                    method: "post",
                    success: function () {
                        location.reload();
                    }
                });
            });
        });
    </script>
</section>