<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of analytics
 *
 * @author Lachu
 */
class Finance extends Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('finance_model'));
        $this->load->library('pagination');
        if (!_can("Finance") || !_is('GR Admin')) {
            redirect(site_url("dashboard"));
        }
    }

    function index() {
        $this->data['states'] = array();
        $this->data['cities'] = array();

        $this->data['maxseats'] = $this->finance_model->getMaxSeats();
        if ($this->input->get()) {
            $filters['seat_range'] = $this->data['seat_range'] = explode(';', $this->input->get('seat_range'));
            if (isset($filters['seat_range'][1]) && isset($filters['seat_range'][0]) && $filters['seat_range'][1] == $this->data['maxseats']->total_seats && $filters['seat_range'][0] == 0) {
                $filters['seat_range'][0] = '';
                $filters['seat_range'][1] = '';
            }
            $this->data['date_range'] = $this->input->get('date_range');
            $range = explode('-', $this->input->get('date_range'));
            $start = 0;
            $end = 0;
            if (isset($range)) {
                if (isset($range[0]) && $range[0] != '') {
                    $start = date(DATE_FORMAT, strtotime($range[0]));
                }
                if (isset($range[1]) && $range[1] != '') {
                    $range[1] = str_replace('?', '', $range[1]);
                    $end = date(DATE_FORMAT, strtotime($range[1] . " + 1 DAY"));
                }
            }
            $filters['from_date'] = $this->data['from_date'] = $start;
            $filters['to_date'] = $this->data['to_date'] = $end;
            $filters['country'] = $this->data['country'] = $this->input->get('country');
            $filters['state'] = $this->data['state'] = $this->input->get('state');
            $filters['city'] = $this->data['city'] = $this->input->get('city');
            $filters['rc_cat'] = $this->data['rc_cat'] = $this->input->get('rc_cat');
            $filters['rc_type'] = $this->data['rc_type'] = $this->input->get('rc_type');
            if ($this->data['country'] != '') {
                $this->data['states'] = $this->finance_model->getStates($this->data['country']);
            }
            if ($this->data['state'] != '') {
                $this->data['cities'] = $this->finance_model->getCities($this->data['state']);
            }
        }
        $this->data['rctypes'] = $this->finance_model->get_types();
        $this->data['rccats'] = $this->finance_model->get_categories();
        $this->data['countries'] = $this->finance_model->getCountries();
        if (isset($filters)) {
            $this->data['analytics'] = $this->finance_model->getAnalytics($filters);
        } else {
            $this->data['analytics'] = $this->finance_model->getAnalytics();
        }
        $homepage = true;
        if ($this->input->get()) {
            if ($this->input->get('loadtype') != '' && $this->input->get('loadtype') != 'html') {
                $homepage = FALSE;
            }
        }
        if ($homepage) {
            $this->gr_template->build('finance_home', $this->data);
        } else if ($this->input->get('loadtype') == 'csv') {
            $array = json_decode(json_encode($this->data['analytics']), true);
            $csv = array(array("Name of Center", "Country", "State", "City", "Address 2", "Address 1", "Pin code", "Total Events", "Total Seats", "Total Bookings"));
            $array = array_merge($csv, $array);
            $this->convert_to_csv($array, 'report.csv', ',');
        } else if ($this->input->get('loadtype') == 'excel') {
            $array = json_decode(json_encode($this->data['analytics']), true);
            $excel = array(array(
                    'center_name' => "Name of Center",
                    'country' => "Country",
                    'state' => "State",
                    'city' => "City",
                    'street_address1' => "Address 2",
                    'street_address2' => "Address 1",
                    'zipcode' => "Pin code",
                    'event_count' => "Total Events",
                    'total_seats' => "Total Seats",
                    'bookedinr' => "Total Bookings INR",
                    'moneyinr' => "Total Amount INR",
                    'bookedusd' => "Total Bookings USD",
                    'moneyusd' => "Total Amount USD"
                )
            );
            $res = array();
            foreach ($array as $value) {
                $dat = array(
                    'bookedinr' => isset($value['booked'][0]['booked']) ? $value['booked'][0]['booked'] : 0,
                    'moneyinr' => isset($value['booked'][0]['fund']) ? $value['booked'][0]['fund'] : 0,
                    'bookedusd' => isset($value['booked'][1]['booked']) ? $value['booked'][0]['booked'] : 0,
                    'moneyusd' => isset($value['booked'][1]['fund']) ? $value['booked'][0]['fund'] : 0
                );
                $value = array_merge($value, $dat);
                $res[] = $value;
            }
            $array = array_merge($excel, $res);
            $this->convert_to_excel($array, "report.xlsx");
        }
    }

    function change_country($country = null) {
        if ($country == '') {
            echo '<option value="">All States</option>';
        } else {
            $country = trim(str_replace('%20', ' ', $country));
            $data = $this->finance_model->getStates($country);
            echo '<option value="">All States</option>';
            foreach ($data as $row) {
                echo '<option value="' . $row->state . '">' . $row->state . '</option>';
            }
        }
    }

    function change_state($state = null) {
        if ($state == '') {
            echo '<option value="">All Cities</option>';
        } else {
            $state = trim(str_replace('%20', ' ', $state));
            $data = $this->finance_model->getCities($state);
            echo '<option value="">All Cities</option>';
            foreach ($data as $row) {
                echo '<option value="' . $row->city . '">' . $row->city . '</option>';
            }
        }
    }

    function getgraphdata($start = null, $end = null) {
        $data = $this->finance_model->getGraphdata($start, $end);
        echo json_encode($data);
    }

    private function convert_to_csv($input_array, $output_file_name, $delimiter) {
        /** open raw memory as file, no need for temp files, be careful not to run out of memory thought */
        $f = fopen('php://memory', 'w');
        /** loop through array  */
        foreach ($input_array as $line) {
            /** default php csv handler * */
            fputcsv($f, $line, $delimiter);
        }
        /** rewrind the "file" with the csv lines * */
        fseek($f, 0);
        /** modify header to be downloadable csv file * */
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
        /** Send file to browser for download */
        fpassthru($f);
    }

    private function convert_to_excel($input_array, $output_file_name) {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $objsheet = $this->excel->getActiveSheet();
        $objsheet->getStyle('B1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('B')->setWidth(25);
        $objsheet->getStyle('C1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('C')->setWidth(25);
        $objsheet->getStyle('D1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('D')->setWidth(25);
        $objsheet->getStyle('E1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('E')->setWidth(25);
        $objsheet->getStyle('F1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('F')->setWidth(25);
        $objsheet->getStyle('G1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('G')->setWidth(25);
        $objsheet->getStyle('H1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('H')->setWidth(25);
        $objsheet->getStyle('I1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('I')->setWidth(25);
        $objsheet->getStyle('J1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('J')->setWidth(25);
        $objsheet->getStyle('K1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('K')->setWidth(25);
        $objsheet->getStyle('L1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('L')->setWidth(25);
        $objsheet->getStyle('M1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('M')->setWidth(25);
        $objsheet->getStyle('N1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('N')->setWidth(25);
        $objsheet->getStyle('O1')->getFont()->setBold(true);
        $objsheet->getColumnDimension('O')->setWidth(25);
        $row111 = 1;
        $i = 0;
        foreach ($input_array as $rowrray) {
            if ($i != 0) {
                $objsheet->setCellValue('A' . $row111, $i);
            }
            $objsheet->setCellValue('B' . $row111, $rowrray['center_name']);
            $objsheet->setCellValue('C' . $row111, $rowrray['country']);
            $objsheet->setCellValue('D' . $row111, $rowrray['state']);
            $objsheet->setCellValue('E' . $row111, $rowrray['city']);
            $objsheet->setCellValue('F' . $row111, $rowrray['street_address1']);
            $objsheet->setCellValue('G' . $row111, $rowrray['street_address2']);
            $objsheet->setCellValue('H' . $row111, $rowrray['zipcode']);
            $objsheet->setCellValue('I' . $row111, $rowrray['event_count']);
            $objsheet->setCellValue('J' . $row111, $rowrray['total_seats']);
            $objsheet->setCellValue('K' . $row111, $rowrray['bookedinr']);
            $objsheet->setCellValue('L' . $row111, $rowrray['moneyinr']);
            $objsheet->setCellValue('M' . $row111, $rowrray['bookedusd']);
            $objsheet->setCellValue('N' . $row111, $rowrray['moneyusd']);
            $row111++;
            $i++;
        }
        header('Content-Type: application/excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $output_file_name . '"');
//tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
//force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function latest_bookings(){
        $content = '';
        $count = '';
        if ($this->input->post()) {
            $content = htmlspecialchars($this->input->post("content"), ENT_QUOTES);
        }
        $total = $this->finance_model->latest_bookings($result = false, 0, 0, $count, $content);

        $config['base_url'] = site_url('finance/latest_bookings');
        $config['total_rows'] = $total;
        $config['per_page'] = $this->session->userdata('list_per_page') ? $this->session->userdata('list_per_page') : 10;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;


        if (isset($content)) {
            $this->data['content'] = $content;
            $this->data['latest_bookings'] = $this->finance_model->latest_bookings($result = true, $config["per_page"], $this->data['page'], FALSE, $content);
        } else {
            $this->data['content'] = NULL;
            $this->data['latest_bookings'] = $this->finance_model->latest_bookings($result = true, $config["per_page"], $this->data['page'], FALSE);
        }
        $this->data['per_page'] = $config['per_page'];
        $this->data['perpages'] = array(10, 25, 50, 100);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->gr_template->build('latest_bookings', $this->data);
    }

    public function change_perpage($perpage = "") {
        if (is_numeric($perpage)) {
            $perpage = (int) $perpage;
        } else {
            $perpage = 10;
        }
        $this->session->set_userdata('list_per_page', $perpage);
        exit();
    }

}

?>
