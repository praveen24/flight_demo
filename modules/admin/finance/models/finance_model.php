<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of analytics_model
 *
 * @author Lachu
 */
class finance_model extends MY_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function getCountries() {
        return $this->db->select('country')
        ->group_by('country')
        ->get('center')->result();
    }

    public function getStates($country) {
        $data = $this->db->select('state')
        ->where('country', $country)
        ->group_by('state')
        ->get('center')->result();
        return $data;
    }

    public function getCities($state) {
        $data = $this->db->select('city')
        ->where('state', $state)
        ->group_by('city')
        ->get('center')->result();
        return $data;
    }

    public function get_types() {
        return $this->db->select('id,name')
        ->from('rc_type')
        ->where('status', 1)
        ->get()
        ->result();
    }

    public function get_categories() {
        return $this->db->select('id,rc_category')
        ->from('rc_category')
        ->where('status', 1)
        ->get()->result();
    }

    public function getMaxSeats() {
        $data = $this->db->select('SUM(total_seats) AS total_seats')
        ->group_by('center_id')
        ->order_by('total_seats', 'DESC')
        ->limit(1, 0)
        ->get('events')
        ->row();
        return $data;
    }

    public function getAnalytics($filters = FALSE) {
        $this->db->select('c.id AS center_id,c.name AS center_name,rc.rc_category AS rc_name,t.name AS type_name,c.country, c.state, c.city, c.street_address1, c.street_address2, c.zipcode,COUNT(e.id) AS event_count,SUM(e.`total_seats`) AS total_seats')
        ->from('center AS c')
        ->join('events AS e', 'c.id=e.center_id', "LEFT")
        ->join('rc_type AS t', 't.id=c.rc_type_id', "LEFT")
        ->join('rc_category AS rc', 'rc.id=c.rc_category_id', "LEFT");
        if (!$filters) {
            
        } else {
            if (isset($filters['seat_range'][0]) && $filters['seat_range'][0] != '') {
                $this->db->having('SUM(e.total_seats) >= ' . $filters['seat_range'][0]);
            }
            if (isset($filters['seat_range'][1]) && $filters['seat_range'][1] != '') {
                $this->db->having('SUM(e.total_seats) <= ' . $filters['seat_range'][1]);
            }

            if ($filters['country'] != '') {
                $this->db->where("c.country", $filters['country']);
            }
            if ($filters['state'] != '') {
                $this->db->where("c.state = ", $filters['state']);
            }
            if ($filters['city'] != '') {
                $this->db->where("c.city = ", $filters['city']);
            }
            if ($filters['rc_cat'] != '') {
                $this->db->where("c.rc_category_id", $filters['rc_cat']);
            }
            if ($filters['rc_type'] != '') {
                $this->db->where("c.rc_type_id", $filters['rc_type']);
            }
        }
        $this->db->where("c.is_deleted", 0);
        $this->db->where("c.status", 1);
        $data1 = $this->db->order_by('e.published_at', "DESC")
        ->group_by('c.id')
        ->get()
        ->result();
        $result = array();
        foreach ($data1 AS $row) {
            $this->db->select('SUM(eo.attend) AS booked,SUM(eo.amount) AS fund,eo.currency')
            ->from('event_orders AS eo')
            ->join('events AS e', 'e.id=eo.event_id')
            ->join('center AS c', 'c.id=e.center_id');
            if ($filters['from_date'] != '') {
                $this->db->where("eo.timestamp >= '" . $filters['from_date'] . "'");
            }
            if ($filters['to_date'] != '') {
                $this->db->where("eo.timestamp <= '" . $filters['to_date'] . "'");
            }
            $data = $this->db->where('c.id', $row->center_id)
            ->where('eo.currency', 'INR')
            ->where('eo.status', 1)
            ->group_by('eo.currency')
            ->get()
            ->result_array();
            $row->booked = $data;
            $result[] = $row;
        }
        return $result;
    }

    public function getGraphdata($start, $end) {
        $this->db->select('DATE(eo.timestamp) AS Date,SUM(eo.attend) AS Bookings')
        ->from('center AS c')
        ->join('events AS e', 'c.id=e.center_id')
        ->join('event_orders AS eo', 'e.id=eo.event_id')
        ->join('rc_type AS t', 't.id=c.rc_type_id')
        ->join('rc_category AS rc', 'rc.id=c.rc_category_id');
        if ($start && $start != 'NaN') {
            $this->db->where("DATE(eo.timestamp) >= '" . date("Y-m-d", ($start / 1000)) . "'");
        }
        if ($end && $end != 'NaN') {
            $this->db->where("DATE(eo.timestamp) <= '" . date("Y-m-d", ($end / 1000)) . "'");
        }

        $this->db->where("c.is_deleted", 0);
        $this->db->where("t.status", 1);
        $this->db->where("eo.status", 1);
        $data = $this->db->order_by('eo.timestamp', "DESC")
        ->group_by('DATE(eo.timestamp)')
        ->get()
        ->result();
        return $data;
    }

    public function latest_bookings($result = true, $perpage, $page, $count, $content = ""){
        $return = $this->db->select('EO.*, C.name as centername, E.name as eventname, E.start_date, E.start_time, E.end_date, E.end_time')
        ->from('event_orders EO')
        ->join('events E', 'E.id=EO.event_id', 'LEFT')
        ->join('center C', 'C.id=E.center_id', 'LEFT')
        ->where('EO.currency', 'INR')
        ->where('EO.status', 1);
        if ($content != NULL) {
            $this->db->where("(E.name like '%$content%' OR EO.toname like '%$content%')", Null,Null);
                        // $this->db->like('E.name', $content);
                        // $this->db->or_like('EO.toname', $content);
        }
        $this->db->order_by('EO.timestamp', "DESC");
        if ($perpage != 0 || $page != 0) {
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        if($result == true){
            return $this->db->get()
            ->result();
        }else{
            return $this->db->get()
            ->num_rows();
        }

    }

}

?>
