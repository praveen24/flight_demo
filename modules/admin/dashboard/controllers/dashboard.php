<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('dashboard_model'));
    }

    function index() {
        if (_is("RC Admin")) {
            $center = $this->session->userdata('current_centre_role')->center_id;
            $this->data['last3events'] = $this->dashboard_model->getLatestEvents($center);
            // $this->data['details'] = $this->dashboard_model->get_event_details($id);
            // $this->data['bookable'] = $this->data['details']->available_seats - $this->bookedCount($id);
            $center_name = $this->dashboard_model->get_centername($center);
            $this->data['center_name'] = $center_name->name;
            $this->data['new_bookings'] = $this->dashboard_model->get_new_bookings($center);
            $new_visitors = $this->dashboard_model->get_new_visitors($center);
            $total_visitors = $this->dashboard_model->get_old_visitors($center);
            $old_visitors = $total_visitors - $new_visitors;
            if ($old_visitors) {
                $percent = ($new_visitors / $old_visitors) * 100;
            } else {
                $percent = 100;
            }
            $this->data['increase_percent'] = $percent;
            $this->data['new_users'] = $new_visitors;
            $this->data['unique_visitors'] = $this->dashboard_model->get_unique_visitors($center);
            $this->gr_template->build('rc_dashboard', $this->data);
        } else if (_is("GR Admin")) {
            $this->data['verificationPending'] = $this->dashboard_model->getPendingVerification();
            $this->data['bankDetailsApprovalPending'] = $this->dashboard_model->getPendingBankDetailsApproval();
            $this->data['center_name'] = "ALL CENTRES";
            $this->data['new_bookings'] = $this->dashboard_model->get_new_bookings();
            $new_visitors = $this->dashboard_model->get_all_visitors();
            $total_visitors = $this->dashboard_model->get_all_old_visitors();
            $old_visitors = $total_visitors - $new_visitors;
            if ($old_visitors) {
                $percent = ($new_visitors / $old_visitors) * 100;
            } else {
                $percent = 100;
            }
            $this->data['increase_percent'] = $percent;
            $this->data['new_users'] = $new_visitors;
            $this->data['unique_visitors'] = $this->dashboard_model->get_all_unique_visitors();
            $this->gr_template->build('rc_dashboard', $this->data);
        } else
        $this->gr_template->build('dashboard', $this->data);
    }

    function get_chart_info() {
        $result = array();
        if (_is('GR Admin')) {
            $result['bookingData'] = $this->dashboard_model->get_all_bookings_by_month();
            $area = $this->dashboard_model->get_all_visitors_by_month();
            $result['area'] = $area;
            $result['line'] = $this->dashboard_model->get_bookings_ByMonth();
            $result['visitorsData'] = $this->dashboard_model->get_bookings_ByLocation();
            $result['siteHits'] = $this->dashboard_model->day_site_hits();
        } else if (_is('RC Admin')) {
            $center = $this->session->userdata('current_centre_role')->center_id;
            $result['bookingData'] = $this->dashboard_model->get_bookings_by_month($center);
            $area = $this->dashboard_model->get_visitors_by_month($center);
            $result['area'] = $area;
            $result['line'] = $this->dashboard_model->get_bookings_ByMonth($center);
            $result['visitorsData'] = $this->dashboard_model->get_bookings_ByLocation();
        }
        echo json_encode($result, JSON_NUMERIC_CHECK);
        exit();
    }

    function hits_per_day(){
        if (_is('RC Admin')) {
            $center_id = $this->session->userdata('current_centre_role')->center_id;
            $result['siteHits'] = $this->dashboard_model->day_center_hits($center_id);
        }else{
            $result['siteHits'] = $this->dashboard_model->day_site_hits();
        }
        echo json_encode($result, JSON_NUMERIC_CHECK);
        exit();
        
    }

    function hits_per_week(){
        if (_is('RC Admin')) {
            $center_id = $this->session->userdata('current_centre_role')->center_id;
            $result['siteHits'] = $this->dashboard_model->week_center_hits($center_id);
        }else{
            $result['siteHits'] = $this->dashboard_model->week_site_hits();
        }
        echo json_encode($result, JSON_NUMERIC_CHECK);
        exit();
        
    }

    function hits_per_month(){
        if (_is('RC Admin')) {
            $center_id = $this->session->userdata('current_centre_role')->center_id;
            $result['siteHits'] = $this->dashboard_model->month_center_hits($center_id);
        }else{
            $result['siteHits'] = $this->dashboard_model->month_site_hits();
        }
        echo json_encode($result, JSON_NUMERIC_CHECK);
        exit();
    }

}
