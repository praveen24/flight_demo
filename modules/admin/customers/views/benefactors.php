<link href="<?= base_url($theme_path . 'plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet"/>
<section class="content-header">
    <h1>
        Benefactors
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= site_url('customers') ?>">Banefactors</a></li>
        <li class="active">List Banefactors</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
</section>
<section class="content">
    <section class="content">
        <div class="row">
            <div class="col-md-12">   
                <div class="box box-success">
                    <div class="box-body">
                        <a href="<?= site_url('customers/add_benefactor') ?>" class="btn btn-success" style="text-align:center;">Add a Benefactor</a><br /><br />
                        <table id="listtypes" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="">Name</th>
                                    <th>Email</th>
                                    <th>Contact number</th>
                                    <th>Benefactor type</th>
                                    <th style="width: 250px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($benefactors as $value) {
                                    ?>
                                    <tr id="type<?= $value->id ?>">

                                        <td>  <span class="desp_name"><?= $value->name ?></span></td>
                                        <td>  <span class="desp_name"><?= $value->email ?></span></td>
                                        <td>  <span class="desp_name"><?= $value->number ?></span></td>
                                        <?php
                                        if ($value->benefactor_type == 1) {
                                            $benefactor_type = 'Platinum';
                                        } else if ($value->benefactor_type == 2) {
                                            $benefactor_type = 'Gold';
                                        } else {
                                            $benefactor_type = 'Silver';
                                        }
                                        ?>
                                        <td>  <span class="desp_name type" ><?= $benefactor_type ?></span></td>
                                        <td> 
                                            <span>
                                                <a href="<?= site_url('customers/update_benefactor') ?>/<?= $value->id ?>" class="editimage btn btn-sm bg-green" title="edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </span>                                                                                                                                                     
                                            <span data-link="<?php echo site_url('customers/delete_benefactor/' . $value->id) ?>" data-namevalue="<?= $value->name ?>" data-id="<?= $value->id ?>" class="deletetype btn btn-sm btn-danger" title="archive">
                                                <i class="fa fa-remove"></i>
                                            </span>  

                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->

    </section>
</section>
<style>
    .btn i{
        height: 10px;
    }
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .pwstrength_viewport_progress{
        display: none;
    }
</style>
<script src="<?= base_url($theme_path . 'plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url($theme_path . 'plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>
<?php
if ($this->session->flashdata('message')) {
    ?>
    <script>
        $(window).load(function () {
            swal({
                title: "",
                text: "<?= $this->session->flashdata('message')['message'] ?>",
                type: "<?= $this->session->flashdata('message')['class'] ?>",
                timer: 2000,
                animation: false,
                showConfirmButton: false
            });
        });

    </script>
    <?php
}
?>
<script>
    $(window).load(function () {
        $("#listtypes").dataTable({
            order: [[0, 'asc']],
            "columnDefs": [
            {"orderable": false, "targets": 4}
            ]
        });
        $(document).on('click', ".deletetype", function () {
            var href = $(this).data('link');
            var id = $(this).data('id');
            var name = $(this).data("namevalue");
            swal({
                title: "Are you sure?",
                text: "You will not be able to undo this action!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, archive it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: href,
                    method: "post",
                    success: function (result) {
                        if (result === '1') {
                            $("#type" + id).remove();
                            swal("Archived!", "Benefactor '" + name + "' archived successfully.", "success");
                        } else {
                            swal("Oops!", "Failed to archive", "error");
                        }
                    }, error: function () {
                        swal("Oops!", "Failed to archive", "error");
                    }
                });

            });
        });
    });
</script>
