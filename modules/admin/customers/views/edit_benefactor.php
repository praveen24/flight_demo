<section class="content-header">
    <h1>
        Edit Benefactor
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= site_url('benefactor') ?>">Benefactors</a></li>
        <li class="active">Edit Benefactor</li>
    </ol>
    <p style="text-align: center;"><a href="javascript:window.history.go(-1);">Go back</a></p>
</section>
<section class="content">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box  box-success">
                    <form name="createuser" method="post" action="<?= site_url('customers/update_benefactor') ?>/<?= $details->id ?>" id="createuser" enctype="multipart/form-data">
                        <?php
                    // debug($details);
                        ?>
                        <div class="box-header">
                            <!--<h3 class="box-title">Condensed Full Width Table</h3>-->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group required <?php
                                    if (form_error('fname') != '')
                                        echo 'has-error';
                                    else
                                        echo '';
                                    ?>">
                                    <label class="h5">Full Name</label>
                                    <input type="text" name="fname" placeholder="Full Name" class="form-control" value="<?= $details->name ?>"/>
                                    <?php echo form_error('fname'); ?>
                                </div>
                                <div class="form-group required <?php
                                if (form_error('email') != '')
                                    echo 'has-error';
                                else
                                    echo '';
                                ?>">
                                <label class="h5">Email</label>
                                <input type="email" name="email" id="email" value="<?= $details->email ?>" placeholder="Email" required class="form-control"/>
                                <?php echo form_error('email'); ?>
                            </div>
                            <div class="form-group required <?php
                            if (form_error('description') != '')
                                echo 'has-error';
                            else
                                echo '';
                            ?>">
                            <label class="h5">Description</label>
                            <textarea name="description" placeholder="Description"  class="form-control" value=""><?= $details->description ?></textarea>
                            <?php echo form_error('description'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required <?php
                        if (form_error('phonenumber') != '')
                            echo 'has-error';
                        else
                            echo '';
                        ?>s">
                        <label class="h5">Phone Number</label>
                        <input type="text" name="phonenumber" id="phonenumber" value="<?= $details->number ?>" placeholder="Contact Number" required class="form-control"/>
                        <?php echo form_error('phonenumber'); ?>
                    </div>
                    <label class="h5">Benefactor type</label>
                    <div class="form-group">
                        <select id="benefactor_type"  class="form-control" name="benefactor_type1">
                            <option value="1" <?=$details->benefactor_type==1?'selected':''?>>Platinum</option>
                            <option value="2" <?=$details->benefactor_type==2?'selected':''?>>Gold</option>
                            <option value="3" <?=$details->benefactor_type==3?'selected':''?>>Silver</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="padding-left: 0px;">

                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label class="h5" for="exampleInputFile">Profile image</label>
                        <span>(preferred image size: 236 * 200)</span>
                        <div class="">
                            <div class="form-group">
                                <?php
                                if (!empty($details->image)) {
                                    $src = base_url($details->image);
                                } else {
                                    $src = base_url($theme_path . 'images/dlogo.jpg');
                                }
                                ?>
                                <div id="imagePreview" style="display: inline-block;background-image: url('')"> <img src="<?php echo $src; ?>"/></div>
                            </div>
                        </div>
                        <input type="hidden" value="" name="uploadImage"/>
                        <button type="button" class="btn btn-default" onclick="" id="uploadimage">Upload</button>
                        <input type="file" name="avatar" id="uploadFile" class="hidden"/>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">  
            <button type="submit" class="btn btn-primary" onclick="return validatethis();">Submit</button>

        </div>
    </form>
</div><!-- /.box -->
</div><!-- /.col -->

</div><!-- /.row -->


<div class="example-modal">
    <div class="modal" id="editModal">
        <div class="modal-dialog">
            <!--<form method="post" enctype="multipart/form-data" action="" id="updateHome">-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Update Benefactor Image</h4>
                </div>
                <div class="modal-body">
                    <div class="" style="width: 100%">
                        <img id="image" class="cropperImage"  style="width: 100%"  src="" alt="Picture">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update_type">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
            <!--</form>  Form end -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.example-modal -->

</section>
</section>

<style>
    #imagePreview {
        width: 100px;
        max-width: 100px;
        background-position: center center;
        background-size: cover;
        /*-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);*/
        /*display: ;*/
    }
    #imagePreview img{
        max-width: 100px;
    }

    #imagePreview canvas{
        width: 100%;
    }
    .pwstrength_viewport_progress{
        display: none;
    }
</style>
<script>
    $(document).ready(function () {
        $('select').chosen();
    });
    $(window).load(function () {
        $('#uploadimage').click(function () {
            $('#uploadFile').click()
        });

        "use strict";
        var options = {};
        options.ui = {
            container: "#pwd-container",
            showVerdictsInsideProgressBar: true,
            viewports: {
                progress: ".pwstrength_viewport_progress"
            }
        };

        $('#uploadFile').change(function () {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                $("#imagePreview").css("display", "none");
                return; // no file selected, or no FileReader support
            }
            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                reader.onloadend = function () { // set image data as background of div
//                    $("#imagePreview").css("display", "inline-block");
//                    $("#imagePreview").css("background-image", "url(" + this.result + ")");
$("#imagePreview").css("display", "inline-block");
$('#image').cropper('destroy');
$('#image').removeAttr('src');
$('#image').attr('src', this.result);
$("#editModal").modal('show');
$('#image').cropper({
    aspectRatio: 236 / 200,
    crop: function (e) {
        $('#update_type').click(function () {
            var canvas = $('#image').cropper('getCroppedCanvas');
            $("#imagePreview").html(canvas);
            $('input[name=uploadImage]').val(canvas.toDataURL());
            $("#editModal").modal('hide');
            ;
        });
    }
});
}
}
});
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen();
        }

        if ('<?php echo $typealert ?>' == 'success') {
            swal("Success!", '<?= $message; ?>', "success");
        } else if ('<?php echo $typealert ?>' == 'error') {
            swal("Oops!", '<?= $message; ?>', "error");

        }
    });
</script>
