<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class customers extends Admin_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('customers_model');
        $this->form_validation->set_error_delimiters('<label class="control-label h5" for="inputError">', '</label>');
        $this->load->library('pagination');
        if (!_can("Customer")) {
            redirect(site_url("dashboard"));
        }
    }

    function index() {
        $this->data['benefactors'] = $this->customers_model->getbenefactors();
        $this->gr_template->build('benefactors', $this->data);
    }

    function delete_benefactor($id) {
        echo $this->customers_model->delete_benefactor($id);
    }

    public function update_benefactor($id) {
        $this->data['typealert'] = "";
        if ($this->input->post()) {
            $this->form_validation->set_rules('fname', 'Name', 'trim|required');
            $this->form_validation->set_rules('phonenumber', 'Mobile Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('benefactor_type1', 'Benefactor Type', 'required');
            if ($this->form_validation->run() == true) {

                if ($_POST['uploadImage'] == '') {
                    $avatar = '';
                } else {

                    //upload and update the file
                    $config['upload_path'] = './uploads/image/benefactors/';

                    if(!file_exists($config['upload_path'])){
                        mkdir($config['upload_path'], 0777, true);
                    }

                    $avatar = '';
                    $image = $this->input->post('uploadImage');
                    if ($image != '') {
                        $imageContent = file_get_contents($image);
                        $file = $config['upload_path'] . uniqid() . '.png';
                        $success = file_put_contents($file, $imageContent);
                        $avatar = $success ? $file : '';
                    }
                }
                $insert_data = array(
                    'name' => $this->input->post('fname'),
                    'number' => $this->input->post('phonenumber'),
                    'email' => $this->input->post('email'),
                    'description' => $this->input->post('description'),
                    'benefactor_type' => $this->input->post('benefactor_type1')
                    );
                if (isset($avatar) && $avatar != '') {
                    $insert_data['image'] = $avatar;
                }
                $this->customers_model->updateWhere(array('id' => $id),$insert_data, 'benefactors');
                $this->data['typealert'] = "success";
                
                $this->data['message'] = '';
            } else {
                $this->data['message'] = '';
            }
        } else {
            $this->data['message'] = '';
        }
        $this->data['details'] = $this->customers_model->getOneWhere(array('id' => $id), 'benefactors');
        $this->gr_template->build('edit_benefactor', $this->data);
    }

    function add_benefactor() {
        $this->data['typealert'] = "";
        if ($this->input->post()) {
            $this->form_validation->set_rules('fname', 'Name', 'trim|required|is_unique[' . IMS_DB_PREFIX . 'benefactors.name]');
            $this->form_validation->set_rules('phonenumber', 'Mobile Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[' . IMS_DB_PREFIX . 'benefactors.email]');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('benefactor_type1', 'Benefactor Type', 'required');
            if ($this->form_validation->run() == true) {

                if ($_POST['uploadImage'] == '') {
                    $avatar = 'uploads/images/eventslider/14520608113861.png';
                } else {

                    //upload and update the file
                    $config['upload_path'] = './uploads/image/benefactors/';

                    if(!file_exists($config['upload_path'])){
                        mkdir($config['upload_path'], 0777, true);
                    }

                    $avatar = '';
                    $image = $this->input->post('uploadImage');
                    if ($image != '') {
                        $imageContent = file_get_contents($image);
                        $file = $config['upload_path'] . uniqid() . '.png';
                        $success = file_put_contents($file, $imageContent);
                        $avatar = $success ? $file : '';
                    }
                }
                $insert_data = array(
                    'name' => $this->input->post('fname'),
                    'number' => $this->input->post('phonenumber'),
                    'email' => $this->input->post('email'),
                    'description' => $this->input->post('description'),
                    'image' => $avatar,
                    'benefactor_type' => $this->input->post('benefactor_type1')
                    );
                $this->customers_model->insertRow($insert_data, 'benefactors');
                $this->data['typealert'] = "success";
                
                $this->data['message'] = '';
            } else {
                $this->data['message'] = '';
            }
        } else {
            $this->data['message'] = '';
        }
        $this->gr_template->build('add_benefactor', $this->data);
    }

}
