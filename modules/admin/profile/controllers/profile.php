<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of chats by admins
 *
 * @author Kunhu
 */
class Profile extends Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('profile_model');
    }

    function index() {
        $id = $this->current_user->id;
        $this->data['details'] = $this->profile_model->profile_details($id);
        $this->gr_template->build('profile', $this->data);
    }

    public function profile_update($id) {
        if ($this->input->post()) {
            $this->form_validation->set_rules('first_name', 'FirstName', 'required|callback_alpha_dash_space');
            $this->form_validation->set_rules('last_name', 'LastName', 'required');
            $this->form_validation->set_rules('phone', 'Mob. No:', 'required|min_length[10]');
            $this->form_validation->set_rules('username', 'UserName', "callback_username_check[$id]|required");
            $this->form_validation->set_rules('email', 'Email', "trim|required|valid_email|callback_useremail_check[$id]");
            if ($this->form_validation->run($this) != FALSE) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'email' => $this->input->post('email'),
                    'contact_number' => $this->input->post('phone'),
                    'username' => $this->input->post('username')
                );
                if (empty($_FILES['newAvatar']['name'])) {
                    
                } else if ($_FILES['newAvatar']['error'] == 0) {
                    $filetype = NULL;
                    //upload and update the file
                    $config['upload_path'] = './uploads/files/profile/';
                    $config['max_size'] = '102428800';
                    $type = $_FILES['newAvatar']['type'];
                    switch ($type) {
                        case 'image/gif':
                        case 'image/jpg':
                        case 'image/png':
                        case 'image/jpeg': {
                                $filetype = 0;
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                break;
                            }
                    }
                    $config['overwrite'] = false;
                    $config['remove_spaces'] = true;
                    if (!file_exists('./uploads/files/profile')) {
                        if (!mkdir('./uploads/files/profile/', 0755, TRUE)) {
                            
                        }
                    }
                    $microtime = microtime(TRUE) * 10000;
                    $this->load->library('upload', $config);

                    if (!$this->upload->do_my_upload('newAvatar', $microtime)) {
                        
                    } else {
                        $data['avatar'] = 'uploads/files/profile/' . $this->upload->file_name;
                    }
                }

                $this->data['update_details'] = $this->profile_model->update_profile_details($id, $data);
            }
            $array = $this->form_validation->error_array();
            if (!$array) {
                $status = 'true';
            } else {
                $status = 'false';
            }
            $result = array('error' => $array, 'status' => $status);
            if ($this->input->is_ajax_request()) {
                echo json_encode($result);
                exit;
            }

            $this->data['details'] = $this->profile_model->profile_details($id);
            $this->gr_template->build('profile', $this->data);
        } else {
            redirect('profile/profile/' . $id);
        }
    }

    function change_pass_profile() {
        $this->session->set_userdata('profile_edit', true);
        redirect('profile/change_pass_page');
    }

    public function change_pass_page() {
        if ($this->input->post('key')) {
            if ($this->gr_auth->checkKey($this->input->post('key'))) {
                $this->checkValidationChangePass();
            }
        } else {
            $this->data['user'] = $this->profile_model->getOneWhere(array('id' => $this->current_user->id), 'user');
            if ($this->input->post()) {
                if (password_verify($this->input->post('current_password'), $this->data['user']->password)) {
                    $this->checkValidationChangePass();
                    $this->data['passworderror'] = false;
                } else {
                    $this->data['passworderror'] = true;
                }
            }
        }

        $this->gr_template->build('change_pass_page', $this->data);
    }

    public function checkValidationChangePass() {
        $this->form_validation->set_rules('password', 'New Password', 'trim|required|callback_password_requirements');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required|matches[password]');
        if ($this->form_validation->run($this) == true) {
            if ($this->gr_auth->reset_password($this->current_user->id, $this->input->post('password'))) {
                $subject = 'Goretreat password reset';

                $message = '<p>Hi ' . $this->data['user']->first_name . ',</p>'
                        . '<p>Your Goretreat password was reset using the email address ' . $this->data['user']->email . ' on ' . date('l, F j, Y') . ' at ' . date('g:ia') . '.</p>'
                        . '<p>If you did this, you can safely disregard this email.</p>'
                        . '<p>If you didn\'t do this, let us know immediately.</p>'
                        . '<p>Thanks,</p>'
                        . '<p>The Goretreat Security Team</p>';

                $this->send_mail($this->data['user']->email, $this->data['user']->first_name . ' ' . $this->data['user']->last_name, $subject, $message);
                // 
                if ($this->session->userdata('profile_edit') != 1) {
                    $this->session->unset_userdata('profile_edit');
                    $this->gr_auth->logout($this->data['user']->id);
                    redirect(site_url('dashboard'));
                } else {
                    $this->session->unset_userdata('profile_edit');
                    redirect('profile');
                }
            }
        } else {
            
        }
    }

}
