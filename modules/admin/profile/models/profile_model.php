<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of search_model
 *
 * @author Lachu
 */
class Profile_model extends MY_Model {

    //put your code here
    public function __construct($table_name = NULL, $primary_key = NULL) {
        parent::__construct($table_name, $primary_key);
    }

    function profile_details($id) {
        return $this->db->select('*')
                        ->from('user')
                        ->where('id', $id)
                        ->get()
                        ->row();
    }

    function update_profile_details($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }

}
