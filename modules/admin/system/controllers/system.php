<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class System extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('gr_auth', IMS_DB_PREFIX . 'user');
        $this->load->model('system_model');
        $this->load->library(array('form_validation'));
        $this->form_validation->set_error_delimiters('', '');

        $this->gr_template
        ->enable_parser(FALSE)
        ->set_theme('admin')
        ->set_layout('login')
        ->set_partial('login_head')
        ->set_partial('login_script');

        $this->data['theme_path'] = 'themes/admin/';

        $language_setting = $this->options_model->getOption('language_setting');
        if (isset($language_setting) && !empty($language_setting)) {
            $this->language = $language_setting['language'];
            $this->lang->load(strtolower(get_class($this)), $this->language);
            $this->lang->load($this->language, 'common/' . $this->language);
        } else {
            $this->lang->load(strtolower(get_class($this)), $this->language);
            $this->lang->load($this->language, 'common/' . $this->language);
        }
    }

    function index() {
        $this->login();
    }

    function login() {
        $this->load->model('admin_common_model');
        if ($this->gr_auth->logged_in()) {
            if ($this->input->get('page_atten')) {
                $user_role_data = $this->options_model->getUserRole($this->session->userdata('user_id'), $this->input->get('centre_id'), $this->input->get('role_id'));
                if ($user_role_data) {

                }
            } else if (isset($_GET['doThing']) && isset($_GET['loID'])) {

            } else {
                redirect(site_url('documentation'));
            }
        }


        $this->data['message'] = '';
        $this->data['error'] = '';

        if ($this->input->post()) {

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == true) {
                    if ($this->gr_auth->login($this->input->post('username'), $this->input->post('password'), null)) {
                        $user_id = $this->session->userdata['user_id'];
                        $roles = $this->admin_common_model->getSingleUserRole($user_id);
                        //check for blog editor
                        if($roles->role_id == 3){
                                redirect(site_url('documentation'), 'refresh');
                        }
                        if (empty($roles)) {
                            $this->data['message'] = "Sorry, there is currently no active role assigned to this user. Please contact Admin for clarification";
                            $logout = $this->gr_auth->logout();
                        } else {
                            $centerActive = $this->gr_auth->check_center_status();
                            $checkRoleIsMaster = $this->admin_common_model->checkRoleIsMaster($roles->role_id);
                            if($checkRoleIsMaster->is_master == 1) {
                                redirect(site_url('documentation'), 'refresh');
                            }
                            elseif (!$centerActive) {
                                $this->data['message'] = "Your Centre has been suspended";
                                $this->gr_auth->logout();
                            } else {
                                $redirect_url_info = $this->session->userdata('redirect_url_info');
                                if (!empty($redirect_url_info)) {
                                    $user_role_data = $this->options_model->getUserRole($this->session->userdata('user_id'), $redirect_url_info['centre_id'], $redirect_url_info['role_id']);
                                    if ($user_role_data) {
                                        $this->session->set_userdata('current_centre_role', $user_role_data);
                                        redirect(site_url('attendance'));
                                    }
                                } else {
                                    $user_role_data = $this->admin_common_model->getCurrentUserRole($this->session->userdata('user_id'), $roles->center_id, $roles->role_id);
                                        if ($roles->role_id == 6 && ($roles->center_id != 0 && $roles->center_id != NULL)) {
                                            $user_role_data = $this->admin_common_model->getCurrentUserRole($this->session->userdata('user_id'), $roles->center_id, $roles->role_id);
                                        }
                                    if ($user_role_data) {
                                        $this->session->set_userdata('current_centre_role', $user_role_data);
                                    }
                                }
                                redirect(site_url('documentation'), 'refresh');
                            }
                        }
                    } else {
                        $this->data['message'] = $this->gr_auth->error();
                    }
            } else {
                $this->data['message'] = validation_errors();
            }
        }
        $this->gr_template->title('Login')->build('login', $this->data);
    }

    function email_send() {
        $this->gr_template->build('email_sent', $this->data);
    }

    function forgot_password($user_id = 0, $code = 0) {

        if ($this->gr_auth->logged_in())
            redirect(site_url('system'));

        if ($user_id == 0) {

            $this->data['message'] = '';

            if ($this->input->post()) {

                $this->form_validation->set_rules('identity', 'Username/Email', 'required');

                if ($this->form_validation->run() == true) {

                    if ($user = $this->gr_auth->forgot_password($this->input->post('identity'))) {

                        $subject = 'You have requested for a new Goretreat password';

                        $message = '<p>Hi ' . $user->first_name . ',</p>'
                        . '<p>You recently asked to reset your Goretreat password.</p>'
                        . '<p><a href="' . site_url('system/forgot_password/' . $user->id . '/' . $user->forgotten_password_code) . '">Click here to change your password.</a></p>'
                        . '<p>Alternatively, you can enter the following password reset code:</p>'
                        . '<p>If you didn\'t request a new password, inform the centre immediately.</p>'
                        . '<p>Thanks,</p>'
                        . '<p>The Goretreat Security Team</p>';
                        $this->send_mail($user->email, $user->first_name . ' ' . $user->last_name, $subject, $message);

                        redirect('system/email_send');
                    } else {
                        $this->data['message'] = '<p style="margin: 10px;">' . $this->gr_auth->error() . '</p>';
                    }
                } else {
                    $this->data['message'] = validation_errors();
                }
            }

            $this->gr_template->build('forgot_password', $this->data);
        } else {

            $this->data['message'] = '';

            $this->data['user'] = $this->gr_auth->password_forgotten_user($user_id);

            if (!$this->data['user']) {
                $this->data['message'] = '<p style="margin: 10px;">' . $this->gr_auth->error() . '</p>';
            } else {
                if ($this->input->post() or $code !== 0) {
                    $this->data['code'] = $this->input->post('code') ? $this->input->post('code') : $code;



                    if ($this->data['user']->forgotten_password_code == $this->data['code']) {
                        $this->gr_auth->set_session($this->data['user']->id);
                        redirect('system/change_pass_page');
                    } else {
                        $this->data['message'] = '<p style="margin: 10px;">Invalid Request.</p>';
                    }
                }
            }

            $this->gr_template->build('forgot_password_step_two', $this->data);
        }
    }

    function change_pass_page() {
        $this->gr_template->build('change_pass_page', $this->data);
    }

    function change_pass() {


        $this->form_validation->set_rules('password', 'New Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == true) {
            if ($this->gr_auth->reset_password($this->session->userdata(user_id), $this->input->post('password'))) {

                $subject = 'Goretreat password reset';

                $message = '<p>Hi ' . $this->data['user']->first_name . ',</p>'
                . '<p>Your Goretreat password was reset using the email address ' . $this->data['user']->email . ' on ' . date('l, F j, Y') . ' at ' . date('g:ia') . '.</p>'
                . '<p>If you did this, you can safely disregard this email.</p>'
                . '<p>If you didn\'t do this, let us know immediately.</p>'
                . '<p>Thanks,</p>'
                . '<p>The Goretreat Security Team</p>';

                $this->send_mail($this->data['user']->email, $this->data['user']->first_name . ' ' . $this->data['user']->last_name, $subject, $message);

                $this->gr_auth->set_session($this->data['user']->id);
                redirect(site_url('system'));
            }
        } else {

            $this->gr_template->build('change_pass_page', $this->data);
        }
    }

    public function activate_user($user_id = 0, $code = NULL) {
        if ($this->gr_auth->logged_in())
            redirect(site_url('system'));
        if ($user_id == 0) {
            $user = NULL;
            $this->data['message'] = '<div class="text-red">Please activate using mail link</div>';
        } else {
            if ($code == NULL) {
                $user = NULL;
            } else {
                $user = $this->gr_auth->getuserFromId($user_id, $code);
            }
            if (!$user) {
                $this->data['message'] = '<div class="text-red">User not found. Please use link sent to your E mail.</div>';
            } else {
                if ($user->active == 1) {
                    $this->data['message'] = '<div class="text-blue">User already activated.</div>';
                } else {
                    $data = array('active' => 1, 'activated_at' => date(DATE_FORMAT), 'activation_code' => NULL);
                    $is_deactivate = $this->system_model->updateUserRow($user_id, $data);
                    if ($is_deactivate)
                        $this->data['message'] = '<div class="text-green">User activated successfully</div>';
                }
            }

            $this->gr_template->build('activate_user_page', $this->data);
        }
    }

    function logout() {
        $logout = $this->gr_auth->logout();
        $this->clear_all_cached_data();
        redirect(site_url('system'), 'refresh');
    }

    public function clear_all_cached_data() {


        $this->session->unset_userdata('selected_year_term_id_teacher');
        $this->session->unset_userdata('selected_progress_report_id_teacher');
        $this->session->unset_userdata('selected_year_term_id_sub_cor');
        $this->session->unset_userdata('selected_progress_report_id_supervisor');
        $this->session->unset_userdata('selected_year_term_id');
        $this->session->unset_userdata('current_centre_role');
        $this->session->unset_userdata('subjects_dropdown');
        $this->session->unset_userdata('subject_dropdown');
        $this->session->unset_userdata('selected_subject');
        $this->session->unset_userdata('assess_category');
        $this->session->unset_userdata('class_dropdown');
        $this->session->unset_userdata('subject_id');
        $this->session->unset_userdata('level_id');
        $this->session->unset_userdata('return');
    }

    function unsubscribe($id, $hash) {
        $this->system_model->removesubscriber($id, $hash);
        $this->session->set_flashdata("message", "Unsubscribed successfully");
        redirect(site_url());
    }

}
