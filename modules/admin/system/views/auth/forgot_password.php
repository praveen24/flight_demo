<form class="form-signin" action="" method="post" style="max-width: 600px">
    <h2 class="form-signin-heading">Forgot Password</h2>  
    <p>Please enter your Email so we can send you an email to reset your password.</p>
    <span class="label label-important" style="width: 100%; margin-bottom: 20px; padding: 0px"><?php echo $message; ?></span>
    <input type="text" id="identity" value="<?php echo set_value('identity'); ?>" name="identity" placeholder="Email/Username" class="input-block-level">
    <input type="submit" value="Continue" name="submit" class="btn btn-primary">
    <a href="<?php echo site_url('admin') ?>" class="btn">Cancel</a>
</form>