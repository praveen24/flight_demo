<div id="da-login">
    <div id="da-login-box-wrapper">
        <div id="da-login-top-shadow">
        </div>
        <div id="da-login-box">
            <div id="da-login-box-header">
                <h1>Password Reset??</h1>
            </div>
            <?php if ($message != ''): ?>
                <div class="da-message error"><?php echo $message; ?></div>
            <?php endif; ?>
            <div id="da-login-box-content">
                <form novalidate="novalidate" id="da-login-form" method="post" action="<?php echo site_url('system/forgot_password'); ?>">
                    <div id="da-login-input-wrapper">
                        <div class="da-login-input">
                            <input name="identity" id="da-login-username" placeholder="Username" type="text" value="<?php echo set_value('identity'); ?>">
                        </div>
                    </div>
                    <div id="da-login-button">
                        <input value="Login" id="da-login-submit" type="submit">
                    </div>
                </form>
            </div>
            <div id="da-login-box-footer">
                <a href="<?php echo site_url('forgot_password') ?>">forgot your password?</a>
                <div id="da-login-tape"></div>
            </div>
        </div>
        <div id="da-login-bottom-shadow">
        </div>
    </div>
</div>