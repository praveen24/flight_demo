<div class="login-box">
    <div class="login-logo" align="center">
        <a><img src="<?php echo base_url($theme_path); ?>/images/logo.png" class="img-responsive" align="center"></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo lang('forgot_password'); ?></p>
        <?php if ($message != ''): ?>
            <div class="text-red"><?php echo $message; ?></div>
        <?php endif; ?>
        <form novalidate="novalidate" id="da-login-form" method="post" action="<?php echo site_url('system/forgot_password'); ?>">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Username/Email" name="identity" value="<?php echo set_value('identity'); ?>"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-5">    
                    <button type="button" onclick="window.location.href = '<?php echo site_url('system'); ?>'" class="btn btn-default btn-block btn-flat"> <?php echo lang('Cancel'); ?></button>
                </div><!-- /.col -->
                <div class="col-xs-2">
                </div>
                <div class="col-xs-5">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo lang('Submit'); ?></button>

                </div><!-- /.col -->
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->