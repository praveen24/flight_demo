
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="login-box">
    <div class="login-logo" align="center">
        <a><img src="<?php echo base_url($theme_path); ?>/images/logo.png" class="img-responsive" align="center"></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"></p>
        <?php if ($message != ''): ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <form novalidate="novalidate" id="da-login-form" method="post" action="">
            <div class="form-group <?= ($error == "error") ? 'has-error' : 'has-feedback' ?>">
                <input type="text" class="form-control" placeholder="Username"name="username" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="Password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-7">    
                    <a href="<?php echo site_url('system/forgot_password') ?>" style="float:left;margin-top:7px;"><?php echo lang('forgot_your_password'); ?></a>
                </div><!-- /.col -->
                <div class="col-xs-5">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo lang('login'); ?></button>
                </div><!-- /.col -->
            </div>
        </form>

    </div><!-- /.login-box-body -->
</div>
