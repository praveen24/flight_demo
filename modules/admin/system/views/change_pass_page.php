<div class="login-box">
    <div class="login-logo">
        <a><h3>Logo</h3></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <h3><?php echo lang('change_password'); ?></h3>
        <form novalidate="novalidate" id="da-login-form" method="post" action="<?php echo site_url('system/change_pass'); ?>">
            <div class="form-group has-feedback">
                <input name="password" placeholder="New Password" type="password" class="form-control"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="text-red"><?php echo form_error('password'); ?></span>
            </div>
            <div class="form-group has-feedback">
                <input name="confirm_password" type="password" class="form-control" placeholder="Confirm Password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="text-red"><?php echo form_error('confirm_password'); ?></span>
            </div>
            <input name="check" type="hidden"/>
            <div class="row">
                <div class="col-xs-7">    

                </div><!-- /.col -->
                <div class="col-xs-5">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo lang('Submit'); ?></button>
                </div><!-- /.col -->
            </div>
        </form>
    </div>
</div>