<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class system_model extends MY_Model {

    //put your code here

    public function __construct() {
        $this->table_name = "user";
        $this->primary_key = 'id';
        parent::__construct();
    }

    public function updateUserRow($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('user', $data);
    }

    public function removesubscriber($id, $hash) {
        return $this->db->where('id', $id)
                        ->where('verification_code', $hash)
                        ->delete('subscribers');
    }

    public function check_verification($id, $code) {
        $data = $this->db->select('*')
        ->from('center')
        ->where('email_verification_code', $code)
        ->where('id', $id)
        ->get();
        $temp = $data->row();
        $update = array('email' => $temp->temporary_email,'email_verification_code' => NULL);
        if ($data->num_rows() > 0) {
            return $this->db->where('id', $id)
            ->update('center', $update);
        } else {
            return false;
        }
    }

}
