<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_home_model
 *
 * @author lenovo
 */
class user_home_model extends MY_Model {

    //put your code here
    public function __construct($table_name = NULL, $primary_key = NULL) {
        parent::__construct($table_name, $primary_key);
    }

    public function getMyBookings($customer_id) {
        $result = $this->db->select('eo.*,e.start_date,e.id AS event_id,e.end_date,e.name AS event_name,rc.name AS center_name,rc.id AS centre_id')
                ->join('events AS e', 'e.id=eo.event_id')
                ->join('center AS rc', 'rc.id=e.center_id')
                ->where('customer_id', $customer_id)
                ->where('eo.status', 1)
                ->order_by('timestamp', 'desc')
                ->get('event_orders AS eo')
                ->result();
        return $result;
    }

    function getMyCancelledBookings($customer_id) {
        $result = $this->db->select('eo.*,e.start_date,e.id AS event_id,e.end_date,e.name AS event_name,rc.name AS center_name,rc.id AS centre_id')
                ->join('events AS e', 'e.id=eo.event_id')
                ->join('center AS rc', 'rc.id=e.center_id')
                ->where('customer_id', $customer_id)
                ->where('eo.status', 0)
                ->order_by('timestamp', 'desc')
                ->get('event_orders AS eo')
                ->result();
        return $result;
    }

    public function getTransaction($id, $customer_id) {
        $eo = $this->db->select('*')
                ->where('customer_id', $customer_id)
                ->where('id', $id)
                ->limit(1)
                ->get('event_orders');
        if ($eo->num_rows() === 1) {
            return $eo->row();
        }
    }

    public function refundTransaction($transact) {
        $from = $transact->currency;
        $from_c = $this->db->select('*')->where('currency_code', $from)->get('countries')->row();
        $deleter = true;
        $wallet1 = $this->db->select('cw.*,c.currency_code')
                ->from('customer_wallet AS cw')
                ->join('countries AS c', 'c.id_countries=cw.country_id', 'left')
                ->where('customer_id', $transact->customer_id)
                ->where('status', 1)
                ->limit(1)
                ->get();
        $row = $wallet1->row();
        if ($row->balance == 0) {
            $this->deleteWhere(array('id' => $row->id), 'customer_wallet');
            $deleter = false;
        }
        if ($wallet1->num_rows() === 1 && $deleter) {
            $wallet = $wallet1->row();
            $to = $wallet->currency_code;
            if ($from != $to) {
                $rate = get_rate_money($from, $to);
                $total = $wallet->balance + ($rate * ($transact->amount - $transact->gateway_charge));
                $delete = $this->db->where('id', $transact->id)
                        ->update('event_orders', array('status' => 0));
                if ($delete) {
                    return $this->db->where('id', $wallet->id)
                                    ->update('customer_wallet', array('balance' => $total));
                }
            } else {
                $total = $wallet->balance + ($transact->amount - $transact->gateway_charge);
                $delete = $this->db->where('id', $transact->id)
                        ->update('event_orders', array('status' => 0));
                if ($delete) {
                    return $this->db->where('id', $wallet->id)
                                    ->update('customer_wallet', array('balance' => $total));
                }
            }
        } else {
            $array = array(
                'customer_id' => $transact->customer_id,
                'balance' => ($transact->amount - $transact->gateway_charge ),
                'country_id' => $from_c->id_countries,
                'status' => 1
            );
            $delete = $this->db->where('id', $transact->id)
                    ->update('event_orders', array('status' => 0));
            if ($delete) {
                return $this->db->insert('customer_wallet', $array);
            }
        }
    }

    function get_rebook_details($bookingID) {
        return $this->db->select('eo.*, oi.gateway_id')
                        ->from('event_orders eo')
                        ->join('order_items oi', 'eo.id = oi.order_id')
                        ->where('eo.id', $bookingID)
                        ->get()
                        ->row();
    }

    function get_booking_details($bookingID) {
        return $this->db->select('eo.*, e.name as eventname, e.start_date, e.end_date, e.start_time, e.end_time, e.description as eventdescription, c.name as centername, c.street_address1, c.street_address2, c.city, c.state as centerstate, c.country as centercountry, a.accomodation_type')
                        ->from('event_orders eo')
                        ->join('events e', 'e.id = eo.event_id')
                        ->join('center c', 'c.id = e.center_id')
                        ->join('accomodation a', 'eo.room_type = a.id')
                        ->where('eo.id', $bookingID)
                        ->get()
                        ->row();
    }

}
