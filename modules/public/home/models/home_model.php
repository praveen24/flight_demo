<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of search_model
 *
 * @author Lachu
 */
class home_model extends MY_Model {

    //put your code here
    public function __construct($table_name = NULL, $primary_key = NULL) {
        parent::__construct($table_name, $primary_key);
    }

    public function searchData($key, $page, $per_page) {
        if ($key != NULL) {
            $count = ($page - 1) * $per_page;
            $query = "SELECT e.id AS event_id,e.name AS event_name,e.image AS event_image,e.start_time AS e_start_time,e.end_time AS e_end_time,e.description AS event_description,e.facilities  AS event_facilities,
            c.id AS center_id,c.name AS center_name,c.logo AS center_image,c.country,c.state,c.city,c.street_address1,c.street_address2,c.zipcode,c.description AS center_description
            FROM center AS c LEFT JOIN events AS e ON c.id = e.center_id WHERE 
            MATCH(e.name,e.start_time,e.end_time,e.description,e.facilities) AGAINST('$key') 
            OR MATCH(c.name,c.country,c.state,c.city,c.street_address1,c.street_address2,c.zipcode,c.description) AGAINST('$key') 
            ORDER BY (e.added_date) DESC LIMIT $count,$per_page";
            $result = $this->db->query($query);
        } else {
            $result = $this->db->select('e.id AS event_id,e.name AS event_name,e.image AS event_image,e.start_time AS e_start_time,e.end_time AS e_end_time,e.description AS event_description,e.facilities  AS event_facilities,
                c.id AS center_id,c.name AS center_name,c.logo AS center_image,c.country,c.state,c.city,c.street_address1,c.street_address2,c.zipcode,c.description AS center_description')
            ->from('events AS e')
            ->join('center AS c', 'e.center_id=c.id')
            ->order_by('added_date', 'DESC')
            ->limit(10)
            ->get();
        }
        return $result->result();
    }

    public function searchCount($key) {
        if ($key != NULL) {
            $query = "SELECT e.id AS event_id
            FROM center AS c LEFT JOIN events AS e ON c.id = e.center_id WHERE 
            MATCH(e.name,e.start_time,e.end_time,e.description,e.facilities) AGAINST('$key') 
            OR MATCH(c.name,c.country,c.state,c.city,c.street_address1,c.street_address2,c.zipcode,c.description) AGAINST('$key')";
            $result = $this->db->query($query);
        } else {
            $result = $this->db->select('*')
            ->order_by('added_date', 'DESC')
            ->limit(10)
            ->get('events');
        }
        return $result->num_rows();
    }

    public function getUpcomingEvents($currentpage = 0) {
        $data = $this->db->select('e.*,c.name AS center_name,c.street_address1,c.street_address2,c.city,c.state')
        ->from('events AS e')
        ->join('center AS c', 'c.id=e.center_id')
        ->where('e.start_date >= NOW()')
        ->where('e.published', 1)
        ->where('c.verified', 1)
        ->where('c.popularity', 1)
        ->where('c.is_deleted', 0)
        ->order_by('e.start_date', 'ASC')
        ->limit(3, $currentpage * 3)
        ->get()->result();
        return $data;
    }

    function get_upcoming_event_count() {
        $data = $this->db->select('COUNT(e.id) AS eventcount')
        ->from('events AS e')
        ->join('center AS c', 'c.id=e.center_id')
        ->where('e.start_date >= NOW()')
        ->where('e.published', 1)
        ->where('c.verified', 1)
        ->where('c.popularity', 1)
        ->where('c.is_deleted', 0)
        ->get()
        ->row();
        return $data->eventcount;
    }

    function getmoreEvents($currentpage = 1) {
        $data = $this->db->select('e.*,c.name AS center_name,c.street_address1,c.state')
        ->from('events AS e')
        ->join('center AS c', 'c.id=e.center_id')
        ->where('e.start_date >= NOW()')
        ->where('e.published', 1)
        ->where('c.is_deleted', 0)
        ->where('c.verified', 1)
        ->where('c.popularity', 1)
        ->order_by('e.start_date', 'ASC')
        ->limit(3, $currentpage * 3)
        ->get();
        if ($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getPopularCenters($currentpage = 0) {
        $data = $this->db->select('c.*,COUNT(e.id) AS eventcount')
        ->from('center AS c')
        ->join('events AS e', 'c.id=e.center_id', "LEFT")
        ->group_by('c.id')
        ->where('verified', 1)
        ->where('popularity', 1)
        ->where('is_deleted', 0)
        ->where('is_public', 0)
        ->where('deleted_on', '0000-00-00 00:00:00')
        ->order_by('rank', 'ASC')
        ->limit(3, $currentpage * 3)
        ->get()->result();
        return $data;
    }

    function get_featured_centers_count() {
        $data = $this->db->select('COUNT(id) AS centercount')
        ->from('center')
        ->where('verified', 1)
        ->where('popularity', 1)
        ->where('is_public', 0)
        ->where('is_deleted', 0)
        ->where('deleted_on', '0000-00-00 00:00:00')
        ->get()->row();
        return $data->centercount;
    }

    public function getLatestArticles($currentpage = 0) {
        $data = $this->db->select('b.*,a.path AS event_image, a.attachment_type')
        ->from('blog AS b')
        ->join('attachment AS a', 'b.id=a.parent_id AND a.parent_type=1 ', "LEFT")
        ->group_by('b.id')
        ->where('b.status', 1)
        ->order_by('b.rank', 'ASC')
        ->limit(4, $currentpage * 4)
        ->get()->result();
        return $data;
    }

    function getmoreCenters($currentpage = 1) {
        $data = $this->db->select('c.*,COUNT(e.id) AS eventcount')
        ->from('center AS c')
        ->join('events AS e', 'c.id=e.center_id', "LEFT")
        ->where('verified', 1)
        ->where('popularity', 1)
        ->where('is_deleted', 0)
        ->where('is_public', 0)
        ->group_by('c.id')
                // ->order_by('eventcount', 'DESC')
        ->order_by('rank', 'ASC')
        ->limit(3, $currentpage * 3)
        ->get();
        if ($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function getPreachers($currentpage = 0) {
        $data = $this->db->select('*')
        ->from('preachers')
        ->where('published', 1)
        ->order_by('rank', 'ASC')
        ->limit(4, $currentpage * 4)
        ->get()->result();
        return $data;
    }

    function get_associated_centre($id) {
        return $this->db->select('c.*')
        ->from('center_preachers as cp')
        ->join('center as c', 'c.id=cp.center_id')
        ->where('cp.preacher_id', $id)
        ->get()
        ->result();
    }

    function get_area_of_expertise($area) {
        return $this->db->select('area_of_expertise')
        ->from('area_of_expertise')
        ->where_in('id', $area)
        ->get()
        ->result();
    }

    function getmorePreachers($currentpage = 1) {
        $data = $this->db->select('*')
        ->from('preachers')
        ->where('published', 1)
        ->order_by('rank', 'ASC')
        ->limit(4, $currentpage * 4)
        ->get();
        if ($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function getmoreArticles($currentpage = 1) {
        $data = $this->db->select('b.*,a.path AS event_image')
        ->from('blog AS b')
        ->join('attachment AS a', 'b.id=a.parent_id AND a.parent_type=1 AND a.attachment_type=0', "LEFT")
        ->group_by('b.id')
        ->order_by('b.rank', 'ASC')
        ->limit(4, $currentpage * 4)
        ->get();
        if ($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function profile_details($id) {
        return $this->db->select('*')
        ->from('customer')
        ->where('id', $id)
        ->get()
        ->row();
    }

    function update_profile_details($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('customer', $data);
    }

    function checkMailexist($id = NULL, $email) {
        if (isset($id) && $id != NULL) {
            $this->db->where('id !=' . $id);
        }

        return $this->db->select('*')
        ->from('customer')
        ->where('email', $email)
        ->get();
    }

    function checkSubscriberexist($email) {
        // if (isset($id) && $id != NULL) {
        //     $this->db->where('id !=' . $id);
        // }

        return $this->db->select('*')
        ->from('subscribers')
        ->where('email', $email)
        ->get();
    }

    function checkusernameexist($id, $name) {

        return $this->db->select('*')
        ->from('customer')
        ->where('id !=' . $id)
        ->where('username', $name)
        ->get();
    }

    public function check_activation($id, $code) {
        $data = $this->db->select('*')
        ->from('customer')
        ->where('activation_code', $code)
        ->where('id', $id)
        ->get();
        $update = array('active' => 1, 'activation_code' => NULL);
        if ($data->num_rows() > 0) {
            return $this->db->where('id', $id)
            ->update('customer', $update);
        } else {
            return false;
        }
    }

    public function list_category() {
        return $this->db->select('*')
        ->from('rc_category')
        ->get()
        ->result();
    }

    public function list_type() {
        return $this->db->select('*')
        ->from('rc_type')
        ->order_by('order', 'ASC')
        ->get()
        ->result();
    }

    //get all slider images for home page
    public function getAllImages() {
        return $this->db->select('*')
        ->from('home_slider')
        ->where('priority !=0')
        ->where('published', 1)
        ->order_by('priority', 'ASC')
        ->limit(10)
        ->get()
        ->result();
    }

    //get homeslider image count
    //get all slider images for home page
    public function getAllImagesCount() {
        $count = $this->db->select('count(*) as count')
        ->from('home_slider')
        ->where('published', 1)
        ->where('priority !=0')
        ->limit(10)
        ->get()
        ->row();
        return $count->count;
    }

    public function toMail() {
        $data = $this->db->select('u.email')
        ->from('user as u')
        ->join('user_role as ur', 'ur.user_id=u.id')
        ->where('ur.role_id', 1)
        ->get()
        ->row();
        return $data;
    }

    public function get_testimonial() {
        $return = array();
        $data = $this->db->select('e.id, c.name, e.image as eventimage, e.description as content, e.name as eventname')
        ->from('reviews as r')
        ->join('events as e', 'e.id=r.item_id')
        ->join('center as c', 'e.center_id=c.id')
        ->where('r.type_id', 1)
        ->group_by('r.item_id')
        ->get()
        ->result();

        foreach ($data as $value) {

            $value->testimonials = $this->db->select('r.*, cm.first_name, cm.avatar as profile ')
            ->from('reviews as r')
            ->join('customer as cm', 'cm.id=r.user_id')
            ->join('events as e', 'e.id=r.item_id')
            ->join('center as c', 'e.center_id=c.id')
            ->where('r.type_id', 1)
            ->where('r.status', 1)
            ->where('e.id', $value->id)
            ->get()
            ->result();
            $return[] = $value;
        }
        return $return;
    }

    public function get_aboutus_details() {
        return $this->db->select('*')
        ->from('footer')
        ->where('name', 'About')
        ->get()
        ->row();
    }

    public function get_benefactor_details() {
       return $this->db->select('b.*, bt.name as b_type')
       ->from('benefactors b')
       ->join('benefactor_type bt', 'b.benefactor_type = bt.id', 'LEFT')
       ->get()
       ->result();
   }

   public function get_faq_details() {
    return $this->db->select('*')
    ->from('footer')
    ->where('name', 'faq')
    ->get()
    ->row();
}

public function get_terms_details() {
    return $this->db->select('*')
    ->from('footer')
    ->where('name', 'Terms')
    ->get()
    ->row();
}

public function get_privacy_details() {
    return $this->db->select('*')
    ->from('footer')
    ->where('name', 'Privacy')
    ->get()
    ->row();
}

public function get_pricing_details() {
    return $this->db->select('*')
    ->from('footer')
    ->where('name', 'pricing')
    ->get()
    ->row();
}

public function get_refund_details() {
    return $this->db->select('*')
    ->from('footer')
    ->where('name', 'refund_policy')
    ->get()
    ->row();
}

public function getCurrentPassword($id) {
    return $this->db->select('*')
    ->from('customer')
    ->where('id', $id)
    ->get()
    ->row();
}

}

?>
