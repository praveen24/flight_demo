<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Twitter extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('twconnect');
    }

    /* show link to connect to Twiiter */

    public function index() {
        
    }

    /* redirect to Twitter for authentication */

    public function redirect() {


        $ok = $this->twconnect->twredirect('twitter/callback');

        if (!$ok) {

            echo '<p>Could not connect to Twitter. Refresh the page or try again later.</p>';
            echo '<p>OR</p>';
            echo '<p>Go to <a href="' . site_url('twitter/redirect') . '">Home Page</a></p>';
        }
    }

    /* return point from Twitter */
    /* you have to call $this->twconnect->twprocess_callback() here! */

    public function callback() {
        $ok = $this->twconnect->twprocess_callback();

        if ($ok) {
            redirect('twitter/success');
        } else
            redirect('twitter/failure');
    }

    /* authentication successful */
    /* it should be a different function from callback */
    /* twconnect library should be re-loaded */
    /* but you can just call this function, not necessarily redirect to it */

    public function success() {

        // saves Twitter user information to $this->twconnect->tw_user_info
        // twaccount_verify_credentials returns the same information
        $this->twconnect->twaccount_verify_credentials();


        $userData = $this->twconnect->tw_user_info;
        $this->session->set_userdata('twitterlogged', 'true');
        $result = $this->gr_auth->login_with_twitter($userData);
        redirect('home');
    }

    /* authentication un-successful */

    public function failure() {
        redirect('home');
    }

    /* clear session */

    public function clearsession() {

        $this->session->sess_destroy();
        redirect('/twitter');
    }

}
